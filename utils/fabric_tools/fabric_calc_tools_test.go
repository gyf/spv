/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package fabric_tools

import (
	"testing"

	"github.com/gogo/protobuf/proto"
	"github.com/stretchr/testify/require"
)

func TestGetEnvelopeByBytes(t *testing.T) {
	env := NewEnvelope()
	envBytes, err := proto.Marshal(env)
	require.NotNil(t, envBytes)
	require.Nil(t, err)

	env1 := GetEnvelopeByBytes(envBytes)
	require.NotNil(t, env1)
}

func TestGetTransactionChannelId(t *testing.T) {
	env := NewEnvelope()
	chainId, err := GetTransactionChannelId(env)
	require.Equal(t, chainId, "mychannel")
	require.Nil(t, err)
}

func TestGetTransactionId(t *testing.T) {
	env := NewEnvelope()
	txId, err := GetTransactionId(env)
	require.Equal(t, txId, "txId")
	require.Nil(t, err)
}

func TestGetTransactionContractData(t *testing.T) {
	env := NewEnvelope()
	name, args, err := GetTransactionContractData(env)
	require.Equal(t, name, "myncc")
	require.Equal(t, string(args[0]), "invoke")
	require.Equal(t, string(args[1]), "a")
	require.Equal(t, string(args[2]), "b")
	require.Equal(t, string(args[3]), "10")
	require.Nil(t, err)
}

func TestGetValidationCodeByMetadata(t *testing.T) {
	block := NewBlock()
	vc := GetValidationCodeByMetadata(0, block.Metadata)
	require.Equal(t, vc, int32(0))
}

func TestGetBlockHash(t *testing.T) {
	block := NewBlock()
	h := GetBlockHash(block.Header)
	require.NotNil(t, h)
}

func TestGetBlockDataHash(t *testing.T) {
	block := NewBlock()
	dh := GetBlockDataHash(block.Data)
	require.Equal(t, dh, block.Header.DataHash)
}
