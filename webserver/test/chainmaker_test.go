/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package test

//import (
//	"bytes"
//	"encoding/json"
//	"fmt"
//	"io/ioutil"
//	"net/http"
//	"testing"
//
//	"chainmaker.org/chainmaker/spv/v2/pb/api"
//	"chainmaker.org/chainmaker/spv/v2/webserver"
//	"github.com/stretchr/testify/require"
//)
//
//const (
//	cmChainId = "chain1"
//)
//
//func Test_validTransaction(t *testing.T) {
//	request := &api.TxValidationRequest{
//		ChainId:     cmChainId,
//		BlockHeight: 7,
//		Index:       -1,
//		TxKey:       "a7861fd190f44d8ca12f31cb6d97929c6332a6172e7341c081dce08734d6b59c",
//		ContractData: &api.ContractData{
//			Name:   "BalanceStable",
//			Method: "Plus",
//			Params: []*api.KVPair{
//				{
//					Key:   "number",
//					Value: []byte("1"),
//				},
//			},
//		},
//		Timeout: 5000,
//		Extra:   nil,
//	}
//	bz, err := json.Marshal(request)
//	require.Nil(t, err)
//	err = sendPostRequest("http://localhost:12346/ValidTransaction", bz)
//	require.Nil(t, err)
//}
//
//func Test_getBlockByHeight(t *testing.T) {
//	wc := &webserver.WithChainIdAndHeight{
//		ChainId:    cmChainId,
//		Height:     1,
//		FromRemote: false,
//	}
//	bz, err := json.Marshal(wc)
//	require.Nil(t, err)
//	err = sendPostRequest("http://localhost:12346/GetBlockByHeight", bz)
//	require.Nil(t, err)
//}
//
//func Test_getBlockByHash(t *testing.T) {
//	wc := &webserver.WithChainIdAndHash{
//		ChainId:    cmChainId,
//		Hash:       "eb7c3f3e0a3791266457f62c9fbf14eae29048754ca0206e62115e18f48c0807",
//		FromRemote: false,
//	}
//	bz, err := json.Marshal(wc)
//	require.Nil(t, err)
//	err = sendPostRequest("http://localhost:12346/GetBlockByHash", bz)
//	require.Nil(t, err)
//}
//
//func Test_getBlockByTxKey(t *testing.T) {
//	wc := &webserver.WithChainIdAndTxKey{
//		ChainId:    cmChainId,
//		TxKey:      "a7861fd190f44d8ca12f31cb6d97929c6332a6172e7341c081dce08734d6b59c",
//		FromRemote: false,
//	}
//	bz, err := json.Marshal(wc)
//	require.Nil(t, err)
//	err = sendPostRequest("http://localhost:12346/GetBlockByTxKey", bz)
//	require.Nil(t, err)
//}
//
//func Test_getLastBlock(t *testing.T) {
//	wc := &webserver.WithChainId{
//		ChainId:    cmChainId,
//		FromRemote: false,
//	}
//	bz, err := json.Marshal(wc)
//	require.Nil(t, err)
//	err = sendPostRequest("http://localhost:12346/GetLastBlock", bz)
//	require.Nil(t, err)
//}
//
//func Test_getTransactionByTxKey(t *testing.T) {
//	wc := &webserver.WithChainIdAndTxKey{
//		ChainId:    cmChainId,
//		TxKey:      "a7861fd190f44d8ca12f31cb6d97929c6332a6172e7341c081dce08734d6b59c",
//		FromRemote: false,
//	}
//	bz, err := json.Marshal(wc)
//	require.Nil(t, err)
//	err = sendPostRequest("http://localhost:12346/GetTransactionByTxKey", bz)
//	require.Nil(t, err)
//}
//
//func Test_getTransactionTotalNum(t *testing.T) {
//	wc := &webserver.WithChainId{
//		ChainId: cmChainId,
//	}
//	bz, err := json.Marshal(wc)
//	require.Nil(t, err)
//	err = sendPostRequest("http://localhost:12346/GetTransactionTotalNum", bz)
//	require.Nil(t, err)
//}
//
//func Test_getBlockTotalNum(t *testing.T) {
//	wc := &webserver.WithChainId{
//		ChainId: cmChainId,
//	}
//	bz, err := json.Marshal(wc)
//	require.Nil(t, err)
//	err = sendPostRequest("http://localhost:12346/GetBlockTotalNum", bz)
//	require.Nil(t, err)
//}
//
//func Test_getCurrentBlockHeight(t *testing.T) {
//	wc := &webserver.WithChainId{
//		ChainId: cmChainId,
//	}
//	bz, err := json.Marshal(wc)
//	require.Nil(t, err)
//	err = sendPostRequest("http://localhost:12346/GetCurrentBlockHeight", bz)
//	require.Nil(t, err)
//}
//
//func sendPostRequest(url string, jsonBz []byte) error {
//	// nolint
//	resp, err := http.Post(url, "application/json", bytes.NewBuffer(jsonBz))
//	if err != nil {
//		return err
//	}
//	data, err := ioutil.ReadAll(resp.Body)
//	if err != nil {
//		return err
//	}
//	fmt.Println(resp)
//	fmt.Println(string(data))
//	return nil
//}
