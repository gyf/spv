/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package test

//import (
//	"bytes"
//	"crypto/tls"
//	"crypto/x509"
//	"encoding/json"
//	"fmt"
//	"io/ioutil"
//	"net/http"
//	"testing"
//
//	"chainmaker.org/chainmaker/spv/v2/pb/api"
//	"github.com/stretchr/testify/require"
//)
//
//func Test_Https(t *testing.T) {
//	pool := x509.NewCertPool()
//	configPath := "/Users/liukemeng/go/src/chainmaker.org/spv/tools/config"
//	caCertPath := configPath + "/tls/ca.pem"
//	certFile := configPath + "/tls/client.pem"
//	keyFile := configPath + "/tls/client.key"
//	//调用ca.crt文件
//	caCrt, err := ioutil.ReadFile(caCertPath)
//	if err != nil {
//		fmt.Println("ReadFile err:", err)
//		return
//	}
//	//解析证书
//	pool.AppendCertsFromPEM(caCrt)
//	cert, err := tls.LoadX509KeyPair(certFile, keyFile)
//	if err != nil {
//		fmt.Println("LoadX509KeyPair error:", err, cert)
//		return
//	}
//	tr := &http.Transport{
//		// 把从服务器传过来的非叶子证书，添加到中间证书的池中，使用设置的根证书和中间证书对叶子证书进行验证。
//		TLSClientConfig: &tls.Config{ // nolint
//			RootCAs: pool,
//			// InsecureSkipVerify: true,
//			Certificates: []tls.Certificate{cert},
//		},
//		//TLSClientConfig: &tls.LocalConfig{InsecureSkipVerify: true}, //InsecureSkipVerify用来控制客户端是否证书和服务器主机名。如果设置为true,//       //则不会校验证书以及证书中的主机名和服务器主机名是否一致。
//	}
//	client := &http.Client{Transport: tr}
//	request := &api.TxValidationRequest{
//		ChainId:     cmChainId,
//		BlockHeight: 2,
//		Index:       -1,
//		TxKey:       "a565f53e12be41cb9fe05db1e77e03c2ff29df7120c3438d97db3decc31aa61f",
//		ContractData: &api.ContractData{
//			Name:   "BalanceStable",
//			Method: "Plus",
//			Params: []*api.KVPair{
//				{
//					Key:   "number",
//					Value: []byte("1"),
//				},
//			},
//		},
//		Timeout: 5000,
//		Extra:   nil,
//	}
//	bz, err := json.Marshal(request)
//	require.Nil(t, err)
//	resp, err := client.Post("https://localhost:12346/ValidTransaction", "application/json", bytes.NewBuffer(bz))
//	require.Nil(t, err)
//	data, err := ioutil.ReadAll(resp.Body)
//	require.Nil(t, err)
//	fmt.Println(resp)
//	fmt.Println(string(data))
//}
