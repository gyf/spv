/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package fabric_prover

import (
	"bytes"
	"fmt"
	"reflect"
	"time"

	"chainmaker.org/chainmaker/spv/v2/adapter"
	"chainmaker.org/chainmaker/spv/v2/coder"
	"chainmaker.org/chainmaker/spv/v2/common"
	"chainmaker.org/chainmaker/spv/v2/logger"
	"chainmaker.org/chainmaker/spv/v2/manager"
	"chainmaker.org/chainmaker/spv/v2/pb/api"
	"chainmaker.org/chainmaker/spv/v2/prover"
	"chainmaker.org/chainmaker/spv/v2/storage"
	"github.com/Rican7/retry"
	"github.com/Rican7/retry/strategy"
	"go.uber.org/zap"
)

const (
	retryCnt       = 10                      // the maximum number of polling transaction
	retryInterval  = 5000                    // the time interval of polling transaction. unit: ms
	defaultTimeout = 5000 * time.Millisecond // the timeout of synchronizing expected block. unit: ms
)

// FabricProver is the implementation of Prover interface for Fabric
type FabricProver struct {
	chainId      string
	sdkAdapter   adapter.SDKAdapter
	coder        coder.Coder
	store        storage.StateDB
	stateManager *manager.StateManager
	log          *zap.SugaredLogger
}

// NewFabricProver creates FabricProver
func NewFabricProver(chainId string, sdkAdapter adapter.SDKAdapter, coder coder.Coder, store storage.StateDB,
	stateManager *manager.StateManager, log *zap.SugaredLogger) *FabricProver {
	if log == nil {
		log = logger.GetLogger(logger.ModuleProver)
	}
	return &FabricProver{
		chainId:      chainId,
		sdkAdapter:   sdkAdapter,
		coder:        coder,
		store:        store,
		stateManager: stateManager,
		log:          log,
	}
}

// VerifyTransaction verifies the existence and validity of the transaction
// nolint
func (p *FabricProver) VerifyTransaction(request *api.TxValidationRequest) error {
	var err error
	// 0. check param
	if err = prover.CheckInfo(p.chainId, request); err != nil {
		p.log.Warnf("[ChainId:%s] prover gets an invalid param! err:%v", p.chainId, err)
		return fmt.Errorf("[ChainId:%s] prover gets an invalid param! err:%v", p.chainId, err)
	}
	blockHeight := request.BlockHeight
	txKey := request.TxKey
	timeout := time.Duration(request.Timeout) * time.Millisecond
	contractData := request.ContractData
	// 1. If the transaction in a higher block than spv local current height,
	// prover will register a listener to listen the missing block.
	_, lastCommittedBlockHeight, ok := p.store.GetLastCommittedBlockHeaderAndHeight(p.chainId)
	if !ok {
		return fmt.Errorf("[ChainId:%s] prover get last committed block height failed", p.chainId)
	}
	if lastCommittedBlockHeight < blockHeight {
		if timeout < defaultTimeout {
			timeout = defaultTimeout
		}
		listener := make(chan uint64, 1)
		ok = p.stateManager.GetBlockManager().RegisterListener(blockHeight, listener)
		if ok {
			p.stateManager.RequestChainHeight()
			select {
			case height := <-listener:
				p.log.Debugf("[ChainId:%s] spv has synced to height:%d", p.chainId, height)
			case <-time.After(timeout):
				p.log.Debugf("[ChainId:%s] spv cannot sync to height:%d before timeout:%dms",
					p.chainId, blockHeight, timeout.Milliseconds())
			}
			p.stateManager.GetBlockManager().RemoveListener(blockHeight)
		}
	}
	// 2. According to TxId obtain the transaction hash and the transaction at the height of the block
	localTxHash, localHeight, ok := p.store.GetTransactionHashAndHeightByTxKey(p.chainId, txKey)
	if !ok {
		return fmt.Errorf("[ChainId:%s]transaction no exist in spv, transaction key:%s, current local height:%d",
			p.chainId, txKey, lastCommittedBlockHeight)
	}
	if blockHeight != localHeight {
		p.log.Warnf("[ChainId:%s] it is an invalid transaction! actual height:%d, you expected height:%d",
			p.chainId, localHeight, blockHeight)
		return fmt.Errorf("[ChainId:%s] it is an invalid transaction! actual height:%d, you expected height:%d",
			p.chainId, localHeight, blockHeight)
	}
	// 3. get transaction by sdk from remote chain
	var (
		txer       common.Transactioner
		remoteHash []byte
	)
	err = retry.Retry(func(uint) error {
		txer, err = p.sdkAdapter.GetTransactionByTxKey(txKey)
		if err != nil {
			p.log.Errorf("[ChainId:%s] prover request transaction failed! txKey:%s, err:%v", p.chainId, txKey, err)
			return err
		}
		// 4. transaction existence validation：verify whether the transaction hash is consistent
		remoteHash, err = txer.GetTransactionHash()
		if err != nil {
			return err
		}

		if !bytes.Equal(localTxHash, remoteHash) {
			p.log.Errorf("[ChainId:%s] prover gets an invalid transaction by SDK! local hash:%s, remote hash:%s",
				p.chainId, localTxHash, remoteHash)
			return fmt.Errorf("[ChainId:%s] prover gets an invalid transaction by SDK! local hash:%s, remote hash:%s",
				p.chainId, localTxHash, remoteHash)
		}
		return nil
	},
		strategy.Limit(retryCnt),
		strategy.Wait(retryInterval*time.Millisecond),
	)
	if err != nil {
		p.log.Errorf("[ChainId:%s] prover can't get transaction by SDK! txKey:%s, err:%v",
			p.chainId, txKey, err)
		return fmt.Errorf(fmt.Sprintf("[ChainId:%s] prover can't get transaction by SDK! txKey:%s, err:%v",
			p.chainId, txKey, err))
	}
	// 5. transaction validity validation：judge whether transaction status code is represented as a valid transaction
	if status := txer.GetStatusCode(); status != 0 {
		p.log.Warnf("[ChainId:%s] it is an invalid transaction! transaction status code:%d", p.chainId, status)
		return fmt.Errorf("[ChainId:%s] it is an invalid transaction! transaction status code:%d", p.chainId, status)
	}
	// 6. contract data validation：judge whether contract data is consistent, including contract name, method, params
	var (
		expectedContractName string
		expectedMethod       string
		expectedParams       []*api.KVPair
	)
	expectedContractName = contractData.Name
	expectedMethod = contractData.Method
	expectedParams = contractData.Params
	actualContractName, err := txer.GetContractName()
	if err != nil {
		return fmt.Errorf("[ChainId:%s] prover gets actual transaction contract name failed, err:%v",
			p.chainId, err)
	}
	actualMethod, err := txer.GetMethod()
	if err != nil {
		return fmt.Errorf("[ChainId:%s] prover gets actual transaction contract method failed, err:%v",
			p.chainId, err)
	}
	actualParameters, err := txer.GetParams()
	if err != nil {
		return fmt.Errorf("[ChainId:%s] prover gets actual transaction contract params failed, err:%v",
			p.chainId, err)
	}
	if actualContractName != expectedContractName {
		p.log.Warnf("[ChainId:%s] invalid transaction! different contract name, actual name:%s, you expected name:%s",
			p.chainId, actualContractName, expectedContractName)
		return fmt.Errorf("[ChainId:%s] invalid transaction! different contract name, actual name:%s, you expected name:%s",
			p.chainId, actualContractName, expectedContractName)
	}
	if actualMethod != expectedMethod {
		p.log.Warnf("[ChainId:%s] it is an invalid transaction! different contract name, "+
			"actual name:%s, you expected name:%s", p.chainId, actualContractName, expectedContractName)
		return fmt.Errorf("[ChainId:%s] it is an invalid transaction! different contract name, "+
			"actual name:%s, you expected name:%s", p.chainId, actualContractName, expectedContractName)
	}
	if len(actualParameters) == 0 && prover.IsEmptyParams(expectedParams) {
		p.log.Infof("[ChainId:%s] prover verifies a valid transaction, TxValidationRequest:%v", p.chainId, request)
		return nil
	}
	if len(actualParameters) != len(expectedParams) {
		p.log.Warnf("[ChainId:%s] it is an invalid transaction! different param len, actual params:%v, "+
			"you expected params:%v", p.chainId, actualParameters, expectedParams)
		return fmt.Errorf("[ChainId:%s] it is an invalid transaction! different param len, actual params:%v, "+
			"you expected params:%v", p.chainId, actualParameters, expectedParams)
	}
	var actualParams [][]byte
	for _, arg := range actualParameters {
		param, ok := arg.([]byte)
		if !ok {
			return fmt.Errorf("[ChainId:%s] prover assert []byte failed, get type:{%s}, want type:{%s}",
				p.chainId, reflect.TypeOf(arg), reflect.TypeOf([]byte{}))
		}
		actualParams = append(actualParams, param)
	}
	for i := 0; i < len(expectedParams) && i < len(actualParams); i++ {
		if !bytes.Equal(actualParams[i], expectedParams[i].Value) {
			p.log.Errorf("[ChainId:%s] it is an invalid transaction! actual param: [%v], you expected param: [%v]",
				p.chainId, actualParams[i], expectedParams[i].Value)
			return fmt.Errorf("[ChainId:%s] it is an invalid transaction! actual param: [%v], you expected param: [%v]",
				p.chainId, actualParams[i], expectedParams[i].Value)
		}
	}
	p.log.Infof("[ChainId:%s] prover verifies a valid transaction, TxValidationRequest:%v", p.chainId, request)
	return nil
}
