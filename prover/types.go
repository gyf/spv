/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package prover

import (
	"errors"
	"fmt"

	"chainmaker.org/chainmaker/spv/v2/pb/api"
)

// CheckInfo judges TxValidationRequest
func CheckInfo(chainId string, request *api.TxValidationRequest) error {
	if request == nil {
		return errors.New("TxValidationRequest should not be nil")
	}
	if request.ChainId != chainId {
		return fmt.Errorf("ChainId should not be %s", request.ChainId)
	}
	if len(request.TxKey) == 0 {
		return errors.New("TxKey should not be empty ")
	}
	if request.ContractData == nil {
		return errors.New("ContractData should not be nil")
	}
	if len(request.ContractData.Name) == 0 {
		return errors.New("ContractName should not be empty ")
	}
	if len(request.ContractData.Method) == 0 {
		return errors.New("Method should not be empty ")
	}
	return nil
}

// IsEmptyParams judges whether the param is empty
func IsEmptyParams(params []*api.KVPair) bool {
	if len(params) == 0 {
		return true
	}
	for _, param := range params {
		if len(param.Key) != 0 || param.Value != nil {
			return false
		}
	}
	return true
}
