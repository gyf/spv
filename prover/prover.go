/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

// Package prover is transaction validation module
package prover

import (
	"chainmaker.org/chainmaker/spv/v2/pb/api"
)

// Prover defines transaction validation interface
type Prover interface {
	// VerifyTransaction verifies the existence and validity of the transaction
	VerifyTransaction(request *api.TxValidationRequest) error
}
