// Code generated by protoc-gen-gogo. DO NOT EDIT.
// source: spv_remoteconfig.proto

package protogo

import (
	fmt "fmt"
	proto "github.com/gogo/protobuf/proto"
	io "io"
	math "math"
	math_bits "math/bits"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.GoGoProtoPackageIsVersion3 // please upgrade the proto package

// RemoteConfig contains chain id and hash type
type RemoteConfig struct {
	ChainId  string `protobuf:"bytes,1,opt,name=chainId,proto3" json:"chainId,omitempty"`
	HashType string `protobuf:"bytes,2,opt,name=hashType,proto3" json:"hashType,omitempty"`
}

func (m *RemoteConfig) Reset()         { *m = RemoteConfig{} }
func (m *RemoteConfig) String() string { return proto.CompactTextString(m) }
func (*RemoteConfig) ProtoMessage()    {}
func (*RemoteConfig) Descriptor() ([]byte, []int) {
	return fileDescriptor_fff76f9d49931153, []int{0}
}
func (m *RemoteConfig) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *RemoteConfig) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_RemoteConfig.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *RemoteConfig) XXX_Merge(src proto.Message) {
	xxx_messageInfo_RemoteConfig.Merge(m, src)
}
func (m *RemoteConfig) XXX_Size() int {
	return m.Size()
}
func (m *RemoteConfig) XXX_DiscardUnknown() {
	xxx_messageInfo_RemoteConfig.DiscardUnknown(m)
}

var xxx_messageInfo_RemoteConfig proto.InternalMessageInfo

func (m *RemoteConfig) GetChainId() string {
	if m != nil {
		return m.ChainId
	}
	return ""
}

func (m *RemoteConfig) GetHashType() string {
	if m != nil {
		return m.HashType
	}
	return ""
}

func init() {
	proto.RegisterType((*RemoteConfig)(nil), "proto.RemoteConfig")
}

func init() { proto.RegisterFile("spv_remoteconfig.proto", fileDescriptor_fff76f9d49931153) }

var fileDescriptor_fff76f9d49931153 = []byte{
	// 140 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x12, 0x2b, 0x2e, 0x28, 0x8b,
	0x2f, 0x4a, 0xcd, 0xcd, 0x2f, 0x49, 0x4d, 0xce, 0xcf, 0x4b, 0xcb, 0x4c, 0xd7, 0x2b, 0x28, 0xca,
	0x2f, 0xc9, 0x17, 0x62, 0x05, 0x53, 0x4a, 0x2e, 0x5c, 0x3c, 0x41, 0x60, 0x49, 0x67, 0xb0, 0xa4,
	0x90, 0x04, 0x17, 0x7b, 0x72, 0x46, 0x62, 0x66, 0x9e, 0x67, 0x8a, 0x04, 0xa3, 0x02, 0xa3, 0x06,
	0x67, 0x10, 0x8c, 0x2b, 0x24, 0xc5, 0xc5, 0x91, 0x91, 0x58, 0x9c, 0x11, 0x52, 0x59, 0x90, 0x2a,
	0xc1, 0x04, 0x96, 0x82, 0xf3, 0x9d, 0x54, 0x4e, 0x3c, 0x92, 0x63, 0xbc, 0xf0, 0x48, 0x8e, 0xf1,
	0xc1, 0x23, 0x39, 0xc6, 0x09, 0x8f, 0xe5, 0x18, 0x2e, 0x3c, 0x96, 0x63, 0xb8, 0xf1, 0x58, 0x8e,
	0x21, 0x8a, 0x4b, 0x4f, 0x4f, 0x1f, 0x6c, 0x53, 0x7a, 0x7e, 0x12, 0x1b, 0x98, 0x61, 0x0c, 0x08,
	0x00, 0x00, 0xff, 0xff, 0xd8, 0x30, 0xde, 0xb6, 0x93, 0x00, 0x00, 0x00,
}

func (m *RemoteConfig) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *RemoteConfig) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *RemoteConfig) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	if len(m.HashType) > 0 {
		i -= len(m.HashType)
		copy(dAtA[i:], m.HashType)
		i = encodeVarintSpvRemoteconfig(dAtA, i, uint64(len(m.HashType)))
		i--
		dAtA[i] = 0x12
	}
	if len(m.ChainId) > 0 {
		i -= len(m.ChainId)
		copy(dAtA[i:], m.ChainId)
		i = encodeVarintSpvRemoteconfig(dAtA, i, uint64(len(m.ChainId)))
		i--
		dAtA[i] = 0xa
	}
	return len(dAtA) - i, nil
}

func encodeVarintSpvRemoteconfig(dAtA []byte, offset int, v uint64) int {
	offset -= sovSpvRemoteconfig(v)
	base := offset
	for v >= 1<<7 {
		dAtA[offset] = uint8(v&0x7f | 0x80)
		v >>= 7
		offset++
	}
	dAtA[offset] = uint8(v)
	return base
}
func (m *RemoteConfig) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	l = len(m.ChainId)
	if l > 0 {
		n += 1 + l + sovSpvRemoteconfig(uint64(l))
	}
	l = len(m.HashType)
	if l > 0 {
		n += 1 + l + sovSpvRemoteconfig(uint64(l))
	}
	return n
}

func sovSpvRemoteconfig(x uint64) (n int) {
	return (math_bits.Len64(x|1) + 6) / 7
}
func sozSpvRemoteconfig(x uint64) (n int) {
	return sovSpvRemoteconfig(uint64((x << 1) ^ uint64((int64(x) >> 63))))
}
func (m *RemoteConfig) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowSpvRemoteconfig
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: RemoteConfig: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: RemoteConfig: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		case 1:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field ChainId", wireType)
			}
			var stringLen uint64
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowSpvRemoteconfig
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				stringLen |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			intStringLen := int(stringLen)
			if intStringLen < 0 {
				return ErrInvalidLengthSpvRemoteconfig
			}
			postIndex := iNdEx + intStringLen
			if postIndex < 0 {
				return ErrInvalidLengthSpvRemoteconfig
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.ChainId = string(dAtA[iNdEx:postIndex])
			iNdEx = postIndex
		case 2:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field HashType", wireType)
			}
			var stringLen uint64
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowSpvRemoteconfig
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				stringLen |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			intStringLen := int(stringLen)
			if intStringLen < 0 {
				return ErrInvalidLengthSpvRemoteconfig
			}
			postIndex := iNdEx + intStringLen
			if postIndex < 0 {
				return ErrInvalidLengthSpvRemoteconfig
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.HashType = string(dAtA[iNdEx:postIndex])
			iNdEx = postIndex
		default:
			iNdEx = preIndex
			skippy, err := skipSpvRemoteconfig(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if (skippy < 0) || (iNdEx+skippy) < 0 {
				return ErrInvalidLengthSpvRemoteconfig
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func skipSpvRemoteconfig(dAtA []byte) (n int, err error) {
	l := len(dAtA)
	iNdEx := 0
	depth := 0
	for iNdEx < l {
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return 0, ErrIntOverflowSpvRemoteconfig
			}
			if iNdEx >= l {
				return 0, io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= (uint64(b) & 0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		wireType := int(wire & 0x7)
		switch wireType {
		case 0:
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return 0, ErrIntOverflowSpvRemoteconfig
				}
				if iNdEx >= l {
					return 0, io.ErrUnexpectedEOF
				}
				iNdEx++
				if dAtA[iNdEx-1] < 0x80 {
					break
				}
			}
		case 1:
			iNdEx += 8
		case 2:
			var length int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return 0, ErrIntOverflowSpvRemoteconfig
				}
				if iNdEx >= l {
					return 0, io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				length |= (int(b) & 0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if length < 0 {
				return 0, ErrInvalidLengthSpvRemoteconfig
			}
			iNdEx += length
		case 3:
			depth++
		case 4:
			if depth == 0 {
				return 0, ErrUnexpectedEndOfGroupSpvRemoteconfig
			}
			depth--
		case 5:
			iNdEx += 4
		default:
			return 0, fmt.Errorf("proto: illegal wireType %d", wireType)
		}
		if iNdEx < 0 {
			return 0, ErrInvalidLengthSpvRemoteconfig
		}
		if depth == 0 {
			return iNdEx, nil
		}
	}
	return 0, io.ErrUnexpectedEOF
}

var (
	ErrInvalidLengthSpvRemoteconfig        = fmt.Errorf("proto: negative length found during unmarshaling")
	ErrIntOverflowSpvRemoteconfig          = fmt.Errorf("proto: integer overflow")
	ErrUnexpectedEndOfGroupSpvRemoteconfig = fmt.Errorf("proto: unexpected end of group")
)
