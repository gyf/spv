/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

syntax = "proto3";

option go_package = "../api";

package proto;

// TxValidationRequest contains tx validation info
message TxValidationRequest {
  string chainId = 1;
  uint64 blockHeight = 2;
  int64 index = 3;
  string txKey = 4;
  ContractData contractData = 5;
  int64 timeout = 6;
  bytes extra = 7;
}

// ContractData contains contract data in the tx
message ContractData {
  string name = 1;
  string version = 2;
  string method = 3;
  repeated KVPair params = 4;
  bytes extra = 5;
}

message KVPair {
  string key = 1;
  bytes value = 2;
}

// TxValidationResponse is the tx validation result
message TxValidationResponse {
  string chainId = 1;
  string txKey = 2;
  Code code = 3;
  string message = 4;
}

enum Code {
  Unknown = 0;
  Valid = 1;
  Invalid = 2;
}

service RpcProver {
  // verify the validation of transaction
  rpc ValidTransaction(TxValidationRequest) returns (TxValidationResponse);
}