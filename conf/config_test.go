/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package conf

import (
	"testing"

	"github.com/spf13/cobra"
	"github.com/stretchr/testify/require"
)

func Test_InitSPVConfig(t *testing.T) {
	err := InitSPVConfig(&cobra.Command{})
	require.Nil(t, err)
}

func Test_InitSPVConfigWithYmlFile(t *testing.T) {
	err := InitSPVConfigWithYmlFile("")
	require.Nil(t, err)
}
