/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package manager

import (
	"context"
	"errors"
	"fmt"
	"sync"

	"chainmaker.org/chainmaker/spv/v2/coder"
	"chainmaker.org/chainmaker/spv/v2/common"
	"chainmaker.org/chainmaker/spv/v2/conf"
	"chainmaker.org/chainmaker/spv/v2/logger"
	"chainmaker.org/chainmaker/spv/v2/storage"
	"chainmaker.org/chainmaker/spv/v2/verifier"
	"go.uber.org/zap"
)

const (
	blockerMapSize = 2048 // default initial capacity of the map for caching synchronized blocks
)

// CallBack will be registered by user
type CallBack func(block common.Blocker)

// BlockManager is the block validation and submission module
type BlockManager struct {
	chainId     string
	state       *State
	verifier    verifier.Verifier
	coder       coder.Coder
	store       storage.StateDB
	callBack    CallBack
	blockerMap  map[uint64]common.Blocker
	blockerCh   chan common.Blocker
	heightMsgCh chan *HeightMsg
	ctx         context.Context

	sync.Mutex
	lastCommittedBlockHeight uint64
	listeners                map[uint64]chan uint64

	log *zap.SugaredLogger
}

// NewBlockManager creates BlockManger
func NewBlockManager(ctx context.Context, chainId string, state *State, verifier verifier.Verifier, coder coder.Coder,
	store storage.StateDB, bCh chan common.Blocker, hCh chan *HeightMsg, log *zap.SugaredLogger) *BlockManager {
	if log == nil {
		log = logger.GetLogger(logger.ModuleBlockManager)
	}
	_, lastHeight, _ := store.GetLastCommittedBlockHeaderAndHeight(chainId)
	return &BlockManager{
		chainId:                  chainId,
		state:                    state,
		verifier:                 verifier,
		coder:                    coder,
		store:                    store,
		blockerMap:               make(map[uint64]common.Blocker, blockerMapSize),
		blockerCh:                bCh,
		heightMsgCh:              hCh,
		ctx:                      ctx,
		lastCommittedBlockHeight: lastHeight,
		listeners:                make(map[uint64]chan uint64),
		log:                      log,
	}
}

// Start startups BlockManager
func (bm *BlockManager) Start() {
	bm.log.Debugf("[ChainId:%s] start block manager!", bm.chainId)
	bm.receiveBlocker()
}

// RegisterListener registers listener the missing block for prover
func (bm *BlockManager) RegisterListener(height uint64, ch chan uint64) bool {
	bm.Lock()
	defer bm.Unlock()
	if height <= bm.lastCommittedBlockHeight {
		return false
	}
	if height > bm.lastCommittedBlockHeight {
		bm.listeners[height] = ch
	}
	return true
}

// RemoveListener removes listener when prover gets the missing block
func (bm *BlockManager) RemoveListener(height uint64) {
	bm.Lock()
	defer bm.Unlock()
	delete(bm.listeners, height)
}

// RegisterCallBack registers CallBack
func (bm *BlockManager) RegisterCallBack(callBack CallBack) error {
	if callBack == nil {
		return errors.New("can not register a nil func")
	}
	bm.callBack = callBack
	return nil
}

// receiveBlocker receives block from blockerCh
func (bm *BlockManager) receiveBlocker() {
	go func() {
		for {
			select {
			case blocker, ok := <-bm.blockerCh:
				if !ok {
					bm.log.Errorf("[ChainId:%s] blockerCh hash been closed!", bm.chainId)
					return
				}
				if err := bm.processBlocker(blocker); err != nil {
					bm.log.Errorf("[ChainId:%s] process block failed!, err:%v", bm.chainId, err)
					return
				}
			case <-bm.ctx.Done():
				return
			}
		}
	}()
}

// processBlocker process block received from blockerCh
func (bm *BlockManager) processBlocker(blocker common.Blocker) error {
	// 1. put the block into blockerMap if none exists
	height := blocker.GetHeight()
	if _, ok := bm.blockerMap[height]; ok {
		bm.log.Debugf("[ChainId:%s] duplicate block! block height:%d", bm.chainId, height)
		return nil
	}
	bm.blockerMap[height] = blocker
	bm.log.Debugf("[ChainId:%s] mapping block! block height:%d", bm.chainId, blocker.GetHeight())
	// 2. commit genesis block not verifying
	if height == 0 {
		delete(bm.blockerMap, height)
		err := bm.commitBlock(blocker)
		if err != nil {
			bm.log.Errorf("[ChainId:%s] commit genesis block failed! block hash:%x, err:%v",
				bm.chainId, blocker.GetBlockHash(), err)
			return err
		}
		// put height to listeners if prover has registered this height
		bm.notifyProver(height)
	}
	// 3. commit all blocks that are synchronized and can be committed
	for {
		currentHeight := bm.state.GetCurrentHeight()
		remoteMaxHeight := bm.state.GetRemoteMaxHeight()
		nextHeight := currentHeight + 1
		if nextHeight > remoteMaxHeight {
			bm.log.Infof("[ChainId:%s] spv has synced to the highest block! current local height:%d, remote max height:%d",
				bm.chainId, currentHeight, remoteMaxHeight)
			break
		}
		block, ok := bm.blockerMap[nextHeight]
		if !ok {
			bm.log.Debugf("[ChainId:%s] missing the next block to commit! next height:%d, current local height:%d",
				bm.chainId, nextHeight, currentHeight)
			break
		}
		// when committing the block of the height is 1, need verify whether the genesis block is committed
		// because the state.currentHeight is 0 when init spv
		if nextHeight == 1 {
			_, ok = bm.store.GetBlockHeaderByHeight(bm.chainId, 0)
			if !ok {
				bm.log.Debugf("[ChainId:%s] missing genesis block! block height:%d, current height:%d",
					bm.chainId, nextHeight, currentHeight)
				break
			}
		}
		delete(bm.blockerMap, nextHeight)
		err := bm.validBlock(block)
		// block is invalid
		if err != nil {
			bm.log.Warnf("[ChainId:%s] invalid block! block height:%d, warn:%v", bm.chainId, nextHeight, err)
			bm.heightMsgCh <- NewHeightMsg(nextHeight, false)
			break
		}
		// block is valid
		err = bm.commitBlock(block)
		if err != nil {
			bm.log.Errorf("[ChainId:%s] commit block failed! block height:%d, block hash:%x, err:%v",
				bm.chainId, block.GetHeight(), block.GetBlockHash(), err)
			return err
		}
		// put height to listeners if prover has registered this height
		bm.notifyProver(block.GetHeight())
	}
	return nil
}

// validBlock verifies block Validity
func (bm *BlockManager) validBlock(blocker common.Blocker) error {
	preBlockHeaderBytes, _, ok := bm.store.GetLastCommittedBlockHeaderAndHeight(bm.chainId)
	if !ok {
		return fmt.Errorf("[ChainId:%s] get last committed block header failed", bm.chainId)
	}
	preBlockHeader, err := bm.coder.DeserializeBlockHeader(preBlockHeaderBytes)
	if err != nil {
		return err
	}
	return bm.verifier.ValidBlock(blocker, preBlockHeader.GetBlockHash())
}

// commitBlock commits block to db
func (bm *BlockManager) commitBlock(block common.Blocker) error {
	// block data need commit
	bd, err := bm.coder.GenerateBlockData(block.GetBlockHeader())
	if err != nil {
		return err
	}
	// transaction data need commit
	td, err := bm.coder.GenerateTransactionData(block)
	if err != nil {
		return err
	}
	// commit block data and transaction data to db
	err = bm.store.CommitBlockDataAndTxData(block.GetChainId(), block.GetHeight(), bd, td)
	if err != nil {
		return err
	}
	// update the current height
	err = bm.state.SetCurrentHeight(block.GetHeight())
	if err != nil {
		bm.log.Errorf("[ChainId:%s] update current local height failed! current local height:%d, block height:%d, err:%v",
			bm.chainId, bm.state.GetCurrentHeight(), block.GetHeight(), err)
		return err
	}
	// process callback
	go func() {
		if bm.callBack != nil {
			bm.callBack(block)
		}
	}()
	if conf.SPVConfig.LogConfig.LogLevel == logger.DEBUG {
		bm.log.Infof("[ChainId:%s] commit block success! block height:%d, block hash:%x",
			bm.chainId, block.GetHeight(), block.GetBlockHash())
	} else {
		if block.GetHeight()%10 == 0 {
			bm.log.Infof("[ChainId:%s] commit block success! block height:%d, block hash:%x",
				bm.chainId, block.GetHeight(), block.GetBlockHash())
		}
	}
	return nil
}

// notifyProver puts height to listeners if prover has registered this height
func (bm *BlockManager) notifyProver(height uint64) {
	bm.Lock()
	defer bm.Unlock()
	bm.lastCommittedBlockHeight = height
	ch, ok := bm.listeners[height]
	if ok {
		ch <- height
		close(ch)
	}
}
