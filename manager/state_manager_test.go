/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package manager

import (
	"fmt"
	"strconv"
	"sync"
	"testing"

	cmCommonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/spv/v2/coder/chainmaker_coder"
	"chainmaker.org/chainmaker/spv/v2/common"
	"chainmaker.org/chainmaker/spv/v2/common/chainmaker_common"
	"chainmaker.org/chainmaker/spv/v2/conf"
	"chainmaker.org/chainmaker/spv/v2/logger"
	"chainmaker.org/chainmaker/spv/v2/mock"
	"chainmaker.org/chainmaker/spv/v2/pb/protogo"
	"chainmaker.org/chainmaker/spv/v2/storage/kvdb/leveldb"
	"chainmaker.org/chainmaker/spv/v2/verifier/chainmaker_verifier"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
)

func TestStateManager_Start(t *testing.T) {
	// 1. init SPV config
	err := conf.InitSPVConfigWithYmlFile("")
	require.Nil(t, err)
	// 2. new verifier, coder and store
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	spvSDK := mock.NewMockSDKAdapter(ctrl)
	defer spvSDK.Stop()
	spvSDK.EXPECT().Stop().AnyTimes().Return(nil)
	spvSDK.EXPECT().SubscribeBlock().Return(
		newBlockCh(0), nil)
	spvSDK.EXPECT().GetChainHeight().AnyTimes().Return(uint64(1), nil)
	spvSDK.EXPECT().GetBlockByHeight(gomock.Any()).AnyTimes().Return(&chainmaker_common.CMBlock{
		Block: newBlock(1),
	}, nil)
	spvSDK.EXPECT().GetTransactionByTxKey(gomock.Any()).AnyTimes().Return(&chainmaker_common.CMTransaction{}, nil)
	spvSDK.EXPECT().GetChainConfig().AnyTimes().Return(&protogo.RemoteConfig{
		ChainId:  "chain1",
		HashType: "SHA256",
	}, nil)
	// 3. new state manager, containing new block manager and new req manager
	spvVerifier := chainmaker_verifier.NewCMSPVVerifier()
	spvCoder := chainmaker_coder.NewCMSPVCoder()
	conf.SPVConfig.StorageConfig.LevelDB.StorePath = "../data/spv_db"
	store := leveldb.NewKvStateDB(conf.SPVConfig.StorageConfig, logger.GetLogger(logger.ModuleStateDB))
	require.NotNil(t, store)

	sm := NewStateManager("chain1", spvSDK, spvVerifier, spvCoder, store, nil)
	require.NotNil(t, sm)

	// 4. start state manager, containing start block manager and start req manager
	err = sm.Start()
	// put a height msg to heightMsgCh
	sm.heightMsgCh <- &HeightMsg{height: 1, isFromRemote: true}
	sm.state.SetRemoteMaxHeight(1)

	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		defer wg.Done()
		for {
			if sm.GetBlockManager().state.GetCurrentHeight() >= 1 {
				fmt.Printf("currentHeight:%d\n", sm.GetBlockManager().state.GetCurrentHeight())
				break
			}
		}
	}()

	wg.Wait()
	require.Nil(t, err)

	// 5. register and remove listener
	ok := sm.blockManger.RegisterListener(uint64(1), make(chan uint64))
	require.Equal(t, ok, false)
	sm.blockManger.RemoveListener(uint64(1))

	// 6. stop state manager
	err = sm.Stop()
	require.Nil(t, err)
}

func newBlock(height uint64) *cmCommonPb.Block {
	block := &cmCommonPb.Block{
		Header: &cmCommonPb.BlockHeader{
			ChainId:      "chain1",
			BlockHeight:  height,
			PreBlockHash: []byte("block_hash"),
			BlockHash:    []byte("block_hash"),
		},
		Dag: nil,
		Txs: []*cmCommonPb.Transaction{
			{Payload: &cmCommonPb.Payload{
				ChainId: "chain1",
				TxId:    strconv.FormatUint(height, 10),
			}},
		},
		AdditionalData: nil,
	}
	return block
}

func newBlockCh(height uint64) chan common.Blocker {
	ch := make(chan common.Blocker, 1)
	block0 := &chainmaker_common.CMBlock{
		Block: newBlock(height),
	}
	ch <- block0
	return ch
}
