/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package manager

import (
	"context"
	"time"

	"chainmaker.org/chainmaker/spv/v2/adapter"
	"chainmaker.org/chainmaker/spv/v2/common"
	"chainmaker.org/chainmaker/spv/v2/conf"
	"chainmaker.org/chainmaker/spv/v2/logger"
	"github.com/Rican7/retry"
	"github.com/Rican7/retry/strategy"
	"go.uber.org/zap"
	"golang.org/x/sync/semaphore"
)

const (
	retryCnt          = 10   // the number of repeated requests for chain config
	retryInterval     = 5000 // the time interval of repeated requests for a block or chain config，unit：ms
	defaultWorkerNums = 100  // the default number of goroutines for concurrent request blocks
)

// ReqManager is the block request module
type ReqManager struct {
	chainId          string
	state            *State
	sdkAdapter       adapter.SDKAdapter
	blockerCh        chan common.Blocker
	heightMsgCh      chan *HeightMsg
	workersSemaphore *semaphore.Weighted
	ctx              context.Context
	log              *zap.SugaredLogger
}

// NewReqManager creates ReqManager
func NewReqManager(ctx context.Context, chainId string, state *State, sdkAdapter adapter.SDKAdapter,
	bCh chan common.Blocker, hCh chan *HeightMsg, log *zap.SugaredLogger) *ReqManager {
	if log == nil {
		log = logger.GetLogger(logger.ModuleReqManager)
	}
	var concurrentNums int32
	chainConfig, err := conf.SPVConfig.GetChainConfig(chainId)
	if err == nil {
		concurrentNums = chainConfig.ConcurrentNums
	}
	if concurrentNums <= 0 {
		concurrentNums = defaultWorkerNums
	}
	return &ReqManager{
		chainId:          chainId,
		state:            state,
		sdkAdapter:       sdkAdapter,
		blockerCh:        bCh,
		heightMsgCh:      hCh,
		workersSemaphore: semaphore.NewWeighted(int64(concurrentNums)),
		ctx:              ctx,
		log:              log,
	}
}

// Start startups ReqManager module
func (rm *ReqManager) Start() {
	rm.log.Debugf("[ChainId:%s] start request manager!", rm.chainId)
	rm.receiveHeightMsg()
}

// receiveHeightMsg receives heightMsg from heightMsgCh
func (rm *ReqManager) receiveHeightMsg() {
	go func() {
		for {
			select {
			case hMsf, ok := <-rm.heightMsgCh:
				if !ok {
					rm.log.Errorf("[ChainId:%s] heightMsgCh has been closed!", rm.chainId)
					return
				}
				if err := rm.processHeightMsg(hMsf); err != nil {
					rm.log.Errorf("[ChainId:%s] process height msg failed! err:%v", rm.chainId, err)
					return
				}
			case <-rm.ctx.Done():
				return
			}
		}
	}()
}

// processHeightMsg process heightMsg received from heightMsgCh
func (rm *ReqManager) processHeightMsg(heightMsg *HeightMsg) error {
	var err error
	height := heightMsg.height
	isFromRemote := heightMsg.isFromRemote
	localHeight := rm.state.GetLocalHeight()
	// process the HeightMsg from remote chain, which needs request blocks from requestedMaxHeight to this height
	if isFromRemote {
		// process genesis block height
		if height == 0 {
			rm.requestBlockByHeight(height)
			return nil
		}
		// process other block height
		if height <= localHeight {
			rm.log.Debugf("[ChainId:%s] expired HeightMsg! local height:%d, block height:%d", rm.chainId, localHeight, height)
			return nil
		}
		for h := localHeight + 1; h <= height; h++ {
			rm.requestBlockByHeight(h)
		}
		err = rm.state.SetRequestedMaxHeight(height)
		if err != nil {
			rm.log.Errorf("[ChainId:%s] update has requested max height failed! local height:%d, new height:%d, err:%v",
				rm.chainId, localHeight, height, err)
			return err
		}
		return nil
	}
	// process the HeightMsg from local by BlockManager, which only need request a block of this height
	rm.requestBlockByHeight(height)
	return nil
}

// requestBlockByHeight requests block by height until it arrives
func (rm *ReqManager) requestBlockByHeight(height uint64) {
	// acquire semaphore
	err := rm.workersSemaphore.Acquire(context.Background(), 1)
	if err != nil {
		rm.log.Errorf("[ChainId:%s] get semaphore failed! block height:%d, err:%v", rm.chainId, height, err)
	}
	go func() {
		var (
			block common.Blocker
			err1  error
		)
		// release semaphore
		defer rm.workersSemaphore.Release(1)
		// always retry
		err1 = retry.Retry(func(uint) error {
			rm.log.Debugf("[ChainId:%s] request block! block height:%d", rm.chainId, height)
			block, err1 = rm.sdkAdapter.GetBlockByHeight(height)
			if err1 != nil {
				rm.log.Errorf("[ChainId:%s] request block failed! block height:%d, err:%v", rm.chainId, height, err1)
				return err
			}
			return nil
		},
			strategy.Wait(retryInterval*time.Millisecond),
		)
		if err1 != nil {
			rm.log.Errorf("[ChainId:%s] can't request and response block by SDK! block height:%d, err:%v",
				rm.chainId, height, err1)
		}
		rm.log.Debugf("[ChainId:%s] response block by request, request height:%d, response height:%d",
			rm.chainId, height, block.GetHeight())
		rm.blockerCh <- block
	}()
}
