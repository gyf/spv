/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package manager

import (
	"fmt"
	"sync"
)

// State caches the important variables during block synchronization
type State struct {
	currentHeight      uint64 // the block height has synchronized and committed by spv
	requestedMaxHeight uint64 // the block height has requested by spv
	remoteMaxHeight    uint64 // the maximum block height of remote chain known to spv
	sync.RWMutex
}

// NewState creates State
func NewState(current uint64, requestedHeight uint64, remoteMaxHeight uint64) *State {
	return &State{
		currentHeight:      current,
		requestedMaxHeight: requestedHeight,
		remoteMaxHeight:    remoteMaxHeight,
	}
}

// GetLocalHeight returns local height
func (s *State) GetLocalHeight() uint64 {
	s.RLock()
	defer s.RUnlock()
	if s.currentHeight > s.requestedMaxHeight {
		return s.currentHeight
	}
	return s.requestedMaxHeight
}

// GetCurrentHeight returns currentHeight
func (s *State) GetCurrentHeight() uint64 {
	s.RLock()
	defer s.RUnlock()
	return s.currentHeight
}

// GetRequestedMaxHeight returns requestedMaxHeight
func (s *State) GetRequestedMaxHeight() uint64 {
	s.RLock()
	defer s.RUnlock()
	return s.requestedMaxHeight
}

// GetRemoteMaxHeight returns remoteMaxHeight
func (s *State) GetRemoteMaxHeight() uint64 {
	s.RLock()
	defer s.RUnlock()
	return s.remoteMaxHeight
}

// SetCurrentHeight sets currentHeight
func (s *State) SetCurrentHeight(newCurrent uint64) error {
	s.Lock()
	defer s.Unlock()
	if newCurrent < s.currentHeight {
		return fmt.Errorf("set current block height failed!, current height:%d, new height:%d", s.currentHeight, newCurrent)
	}
	s.currentHeight = newCurrent
	return nil
}

// SetRequestedMaxHeight sets requestedMaxHeight
func (s *State) SetRequestedMaxHeight(newRequestHeight uint64) error {
	s.Lock()
	defer s.Unlock()
	if newRequestHeight < s.requestedMaxHeight {
		return fmt.Errorf("set has requested max height failed!, has requested max height:%d, new height:%d",
			s.requestedMaxHeight, newRequestHeight)
	}
	s.requestedMaxHeight = newRequestHeight
	return nil
}

// SetRemoteMaxHeight sets remoteMaxHeight
func (s *State) SetRemoteMaxHeight(newRemote uint64) error {
	s.Lock()
	defer s.Unlock()
	if newRemote < s.remoteMaxHeight {
		return fmt.Errorf("set remote block height failed!, remote max height:%d, new height:%d",
			s.remoteMaxHeight, newRemote)
	}
	s.remoteMaxHeight = newRemote
	return nil
}
