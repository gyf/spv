/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package kvdb

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestKvDBBatcher_Func(t *testing.T) {
	batcher := NewKvDBBatcher()
	batcher.Add("hello", []byte("word"))
	kvs := batcher.GetKvs()
	require.NotNil(t, kvs)

	l := batcher.Len()
	require.Equal(t, l, 1)

}

func TestKv_Func(t *testing.T) {
	kv := NewKv("hello", []byte("word"))
	k := kv.GetKey()
	require.NotNil(t, k)

	v := kv.GetValue()
	require.NotNil(t, v)
}
