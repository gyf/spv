/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

// Package storage is storage module of all chains
package storage

// StateDB defines storage interface
type StateDB interface {
	// CommitBlockDataAndTxData commits block data and transaction data in an atomic operation
	CommitBlockDataAndTxData(chainId string, height uint64, blockData *BlockData, txData *TransactionData) error

	// GetBlockHeaderByHeight returns the serialized block header given it's height, or returns nil if none exists
	GetBlockHeaderByHeight(chainId string, height uint64) ([]byte, bool)

	// GetBlockHeaderAndHeightByHash returns the serialized block header and height given it's block hash,
	// or returns nil if none exists
	GetBlockHeaderAndHeightByHash(chainId string, blockHash []byte) ([]byte, uint64, bool)

	// GetLastCommittedBlockHeaderAndHeight returns the last committed block header,or returns nil if none exists
	GetLastCommittedBlockHeaderAndHeight(chainId string) ([]byte, uint64, bool)

	// GetTransactionHashAndHeightByTxKey returns transaction hash and block height by txId, or returns nil if none exists
	GetTransactionHashAndHeightByTxKey(chainId string, txKey string) ([]byte, uint64, bool)

	// GetTransactionByTxKey returns transaction bytes by txId, or returns nil if none exists
	GetTransactionByTxKey(chainId string, txKey string) ([]byte, bool)

	// GetTxExtraDataByTxKey returns transaction extra data by txId, or returns nil if none exists
	GetTxExtraDataByTxKey(chainId string, txKey string) ([]byte, bool)

	// GetTransactionTotalNum returns the total num of transactions, or returns -1 if none exists
	GetTransactionTotalNum(chainId string) (uint64, bool)

	// WriteChainConfig put the chain config to db
	WriteChainConfig(chainId string, chainConfig []byte) error

	// GetChainConfig returns chain config, or returns nil if none exists
	GetChainConfig(chainId string) ([]byte, bool)

	// Close is used to close database
	Close()
}
