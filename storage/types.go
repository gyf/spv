/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package storage

// BlockData contains block hash and serialized block header for committing to db
type BlockData struct {
	// block hash
	BlockHash []byte
	// serialized block header
	SerializedHeader []byte
}

// NewBlockData creates BlockData
func NewBlockData(blockHash []byte, serializedHeader []byte) *BlockData {
	return &BlockData{
		BlockHash:        blockHash,
		SerializedHeader: serializedHeader,
	}
}

// TransactionData contains TxHashMap TxBytesMap and ExtraDataMap for committing to db
type TransactionData struct {
	// map[TxId] = TxHash
	TxHashMap map[string][]byte
	// map[TxId] = TxBytes
	TxBytesMap map[string][]byte
	// map[TxId] = TxExtraDataBytes
	ExtraDataMap map[string][]byte
}

// NewTransactionData creates TransactionData
func NewTransactionData() *TransactionData {
	return &TransactionData{
		TxHashMap:    map[string][]byte{},
		TxBytesMap:   map[string][]byte{},
		ExtraDataMap: map[string][]byte{},
	}
}
