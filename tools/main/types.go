/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package main

type TxInfo struct {
	ChainId      string        `mapstructure:"chain_id"`
	BlockHeight  int           `mapstructure:"block_height"`
	TxKey        string        `mapstructure:"tx_key"`
	ContractData *ContractData `mapstructure:"contract_valid_data"`
	Timestamp    int64         `mapstructure:"timestamp"`
}

type ContractData struct {
	Name   string    `mapstructure:"name"`
	Method string    `mapstructure:"method"`
	Params []*Params `mapstructure:"parameters"`
}

type Params struct {
	Key   string `mapstructure:"key"`
	Value string `mapstructure:"value"`
}
