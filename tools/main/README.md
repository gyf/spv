## SPV tools使用说明

### 前置需求：

- 部署业务链
- 部署SPV

上述需求请参考SPV部署文档 - [快速部署文档](../../docs/deploy_manual.md)


### SPV中tools使用方式：
- 将build/release/config/chainmaker或fabric中的crypto-config复制到tools/config/chainmaker或fabric目录下
- 修改tools/config/chainmaker或fabric下的chainmaker_sdk.yml或fabric_sdk.yml中的配置文件
- 修改main.go中chainmaker或fabric中相关配置参数，如channelId、nodeAddress、证书路径等
- 编译tools中的main.go, 生成main可执行文件

### ChainMaker工具：
```shell script
## 部署GASM合约
./main -step 0


## 调用GASM合约，发送交易,forward表示是否通过spv轻节点转发，addr_port表示grpc地址，enable_tls表示是否开启tls验证，enable_ca是否开启ca验证
./main -step=1 -forward=false -addr_port="127.0.0.1:12345" -enable_tls=false -enable_ca=false


## 部署EVM合约
./main -step 2


## 调用EVM合约，发送交易   
./main -step 3 -addr="" -balance=x           // addr需要传入数字类型的的字符串


## 获取某一高度区块中第一笔交易的状态码和TxId    
./main -step 4 -height=h                     // h是需要输入的区块高度


## GASM合约交易验证
./main -step=5 -addr_port="127.0.0.1:12345" -enable_tls=false -enable_ca=false -tx_info_path="../config/params.yml" -run_time=4


## EVM合约交易验证    需要在tools/config/params.yml中填写验证的交易信息
./main -step=5 -addr_port="127.0.0.1:12345" -enable_tls=false -enable_ca=false -tx_info_path="../config/params.yml" -run_time=5 -abi_path="../contract/ledger_balance.abi"


# 交易有效性验证返回值    需要在tools/config/params.yml中填写验证的交易信息
{"ChainId":"chain1","TxKey":"3dc6fc2c91564103835800e13f8a3d761b23dd7008c64cb7a4f530082b4e7623","Code":1,"Message":"valid transaction"}%

其中：
ChainId：链Id
TxKey: 交易Id
Code: 状态码,交易有效为 1, 交易无效为 2
Message: 提示信息
```

### Fabric工具（byfn示例）：
```shell script
## 查询余额
 ./main -step=10 -e2e_from="a"


## 转账交易
 ./main -step=11 -e2e_from="a" -e2e_to="b" -e2e_val="10"


## 订阅区块
./main -step=12


## 根据高度查询区块
./main -step=13 -num=n
 
 
## 查询交易  
./main -step=14 -tx_id=x


## 查询远端链高度        
./main -step=15


## 验证Fabric交易有效性
./main -step 16 -addr_port="127.0.0.1:12345" -enable_tls=false -enable_ca=false -tx_info_path="../config/params.yml"


# 交易有效性验证返回值    需要在tools/config/params.yml中填写验证的交易信息
{"ChainId":"chain1","TxKey":"3dc6fc2c91564103835800e13f8a3d761b23dd7008c64cb7a4f530082b4e7623","Code":1,"Message":"valid transaction"}%

其中：
ChainId：链Id
TxKey: 交易Id
Code: 状态码,交易有效为1, 交易无效为 2
Message: 提示信息
```