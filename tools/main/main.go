/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

// Package main implements a test tool
package main

import (
	"context"
	"crypto/sha256"
	"crypto/tls"
	"crypto/x509"
	"encoding/asn1"
	"encoding/hex"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"math/big"
	"path/filepath"
	"strconv"
	"strings"
	"sync"

	"chainmaker.org/chainmaker/common/v2/evmutils"
	"chainmaker.org/chainmaker/common/v2/json"
	cmCommonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	cmSDK "chainmaker.org/chainmaker/sdk-go/v2"
	"chainmaker.org/chainmaker/sdk-go/v2/examples"
	"chainmaker.org/chainmaker/spv/v2/pb/api"
	"chainmaker.org/chainmaker/spv/v2/pb/protogo/cm_pbgo"
	"chainmaker.org/chainmaker/spv/v2/utils/chainmaker_tools"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/golang/protobuf/proto" // nolint
	fabCommonPb "github.com/hyperledger/fabric-protos-go/common"
	fabPeerPb "github.com/hyperledger/fabric-protos-go/peer"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/channel"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/event"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/ledger"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/providers/fab"
	"github.com/hyperledger/fabric-sdk-go/pkg/core/config"
	"github.com/hyperledger/fabric-sdk-go/pkg/fab/events/deliverclient/seek"
	"github.com/hyperledger/fabric-sdk-go/pkg/fabsdk"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

// ChainMaker配置
const (
	// sdk需要配置的内容
	cmSDKYmlFile = "../config/chainmaker/chainmaker_sdk.yml"
	adminKeyPath = "../config/chainmaker/crypto-config/%s/user/admin1/admin1.tls.key"
	adminCrtPath = "../config/chainmaker/crypto-config/%s/user/admin1/admin1.tls.crt"
	// tls
	tlsClientCert = "../config/tls/client.pem"
	tlsClientKey  = "../config/tls/client.key"
	tlsCa         = "../config/tls/ca.pem"
)

var orgs = []string{
	"wx-org1.chainmaker.org",
	"wx-org2.chainmaker.org",
	"wx-org3.chainmaker.org",
	"wx-org4.chainmaker.org",
}
var (
	grpcAP     = "127.0.0.1:12345"
	txInfoPath = "../config/params.yml"
	abiPath    = "../contract/ledger_balance.abi"
	runTime    = int(cmCommonPb.RuntimeType_GASM)
	height     = 0
	address    = "1"
	balance    = 1
	forward    = false
	enableTls  = false
	enableCa   = false
)

// Fabric 配置参数
const (
	fabSDKYmlFile = "../config/fabric/fabric_sdk.yml"
	channelId     = "mychannel"
	user          = "user"
	peer0org1     = "peer0.org1.example.com"
	peer1org1     = "peer1.org1.example.com"
	peer0org2     = "peer0.org2.example.com"
	peer1org2     = "peer1.org2.example.com"
)

var (
	e2eFrom = "a"
	e2eTo   = "b"
	e2eVal  = "10"
	txId    = ""
	number  = 0
)

func main() {
	var (
		step int
	)
	flag.IntVar(&step, "step", 1, "0: deploy wasm contract, 1: invoke wasm contract, "+
		"2: deploy evm contract, 3: invoke evm contract, 4: get transaction info, 5: verify transaction, "+
		"10: query balance, 11: invoke e2e fabric contract, 12: subscribe Block, 13: query block, 14: query transaction, "+
		"15: query remote height, 16: verify fabric transaction, 17: subscribe chaincode event")
	// ChainMaker flag
	flag.StringVar(&grpcAP, "addr_port", grpcAP, "grpc address/port, default use 127.0.0.1:12308")
	flag.StringVar(&txInfoPath, "tx_info_path", txInfoPath, "txInfo path, default use ../config/params.yml")
	flag.StringVar(&abiPath, "abi_path", abiPath, "evm abi path, default use ../contract/ledger_balance.abi")
	flag.StringVar(&address, "addr", address, "user address , default use addr")
	flag.IntVar(&balance, "balance", balance, "balance value, default use 0")
	flag.IntVar(&runTime, "run_time", runTime, "run time type, default use RuntimeType_GASM")
	flag.IntVar(&height, "height", height, "transaction block height, default use 0")
	flag.BoolVar(&forward, "forward", forward, "forward transaction or not, default use false")
	flag.BoolVar(&enableTls, "enable_tls", enableCa, "enable tls, default use false")
	flag.BoolVar(&enableCa, "enable_ca", enableCa, "enable ca auth, default use false")

	// Fabric flag
	flag.StringVar(&e2eFrom, "e2e_from", e2eFrom, "transfer from this account, default use a")
	flag.StringVar(&e2eTo, "e2e_to", e2eTo, "transfer to this account, default use b")
	flag.StringVar(&e2eVal, "e2e_val", e2eVal, "transfer value, default use 10")
	flag.StringVar(&txId, "tx_id", txId, "transaction Id, default use \"\" ")
	flag.IntVar(&number, "num", number, "block number, default use 0")
	flag.Parse()

	switch step {
	// ChainMaker Func
	case 0: //部署GASM合约（部署合约的交易类型为：TxType_MANAGE_USER_CONTRACT）            ./main -step=0
		createGASMContract()
	case 1: //调用GASM合约，发送交易（调用用户合约的交易类型：TxType_INVOKE_USER_CONTRACT）
		// ./main -step=1 -forward=false -addr_port="127.0.0.1:12345" -enable_tls=false -enable_ca=false
		sendGASMTx()
	case 2: //部署EVM合约                                                             ./main -step=2
		createEVMContract()
	case 3: //调用EVM合约，发送交易                                                     ./main -step=3 -addr="" -balance=x
		sendEVMTx()
	case 4: //获取某一高度区块中第一笔交易的TxId                                          ./main -step=4 -height=h
		getTxInfo()
	case 5: //验证GASM合约交易有效性
		// ./main -step=5 -addr_port="127.0.0.1:12345" -enable_tls=false -enable_ca=false -tx_info_path="../config/params.yml" -run_time=4
		//验证EVM合约交易有效性
		// nolint
		// ./main -step=5 -addr_port="127.0.0.1:12345" -enable_tls=false -enable_ca=false -tx_info_path="../config/params.yml" -run_time=5 -abi_path="../contract/ledger_balance.abi"
		verifyTransaction()

	// Fabric Func
	case 10: //查询余额             ./main -step=10 -e2e_from="a"
		queryBalance()
	case 11: //转账交易             ./main -step=11 -e2e_from="a" -e2e_to="b" -e2e_val="10"
		invokeSendTx()
	case 12: //订阅区块             ./main -step=12
		subscribeBlock()
	case 13: //根据高度查询区块      ./main -step=13 -num=n
		queryBlock()
	case 14: //查询交易             ./main -step=14 -tx_id=x
		queryTransaction()
	case 15: //查询远端链高度        ./main -step=15
		queryHeight()
	case 16: //验证Fabric交易有效性
		// nolint
		// ./main -step 16 -addr_port="127.0.0.1:12345" -enable_tls=false -enable_ca=false -tx_info_path="../config/params.yml"
		verifyFabricTransaction()
	case 17: //订阅ChainCode事件    ./main -step=17
		subscribeCCEvent()

	default:
		panic("step is not exit!")
	}
}

// ChainMaker Func
func createGASMContract() {
	// 创建ChainClient
	client := newCMSDKClient(cmSDKYmlFile)
	fmt.Println("====================== 创建GASM合约 ======================")
	// 构造Payload
	byteCodePath, err := filepath.Abs("../contract/balance.wasm")
	if err != nil {
		fmt.Printf("read wasm contract bytecode path failed, err:%v\n", err)
		return
	}
	payload, err := client.CreateContractCreatePayload("BalanceStable", "1.0.1", byteCodePath,
		cmCommonPb.RuntimeType_GASM, []*cmCommonPb.KeyValuePair{})
	if err != nil {
		fmt.Printf("CreateContractCreatePayload failed, err:%v\n", err)
		return
	}
	// 构造EndorsementEntry
	var endorsers []*cmCommonPb.EndorsementEntry
	var entry *cmCommonPb.EndorsementEntry
	for _, orgId := range orgs {
		entry, err = cmSDK.SignPayloadWithPath(fmt.Sprintf(adminKeyPath, orgId), fmt.Sprintf(adminCrtPath, orgId), payload)
		if err != nil {
			fmt.Printf("SignPayloadWithPath failed, err:%v\n", err)
			return
		}
		endorsers = append(endorsers, entry)
	}
	// 发送创建合约请求
	resp, err := client.SendContractManageRequest(payload, endorsers, 10, true)
	if err != nil {
		fmt.Printf("SendContractManageRequest failed, err:%v\n", err)
		return
	}
	// 转为json string
	respStr, err := json.Marshal(resp)
	if err != nil {
		fmt.Printf("MarshalResponse failed, err:%v\n", err)
		return
	}
	fmt.Printf("%s", respStr)
}

func sendGASMTx() {
	// 创建ChainClient
	client := newCMSDKClient(cmSDKYmlFile)
	// 交易转发逻辑
	if forward {
		fmt.Println("====================== 转发交易，调用GASM合约 ======================")
		// 构造TxRequest
		req, err := client.GetTxRequest("BalanceStable", "Plus", "",
			[]*cmCommonPb.KeyValuePair{{Key: "number", Value: []byte("1")}})
		if err != nil {
			fmt.Printf("GetTxRequest failed, err:%v\n", err)
			return
		}
		reqBytes, err := proto.Marshal(req) // nolint
		if err != nil {
			fmt.Printf("Marshal TxRequest failed, err:%v\n", err)
			return
		}
		request := &cm_pbgo.TxRequest{}
		err = proto.Unmarshal(reqBytes, request)
		if err != nil {
			fmt.Printf("Unmarshal TxRequest failed, err:%v\n", err)
			return
		}
		// 构造ForwardClient并转发交易
		conn, forwardClient := newRpcForwarderClient()
		defer conn.Close()
		resp, err := forwardClient.ForwardRequest(context.Background(), request) // nolint
		respStr, err := json.Marshal(resp)                                       // nolint
		if err != nil {
			fmt.Printf("MarshalResponse failed, err:%v", err)
			return
		}
		fmt.Printf("%s", respStr)
		return
	}
	// 不转发，直接调用sdk
	fmt.Println("====================== 发送交易，调用GASM合约 ======================")
	resp, err := client.InvokeContract("BalanceStable", "Plus", "",
		[]*cmCommonPb.KeyValuePair{{Key: "number", Value: []byte("1")}}, -1, true)
	if err != nil {
		fmt.Printf("InvokeContract failed, err:%v\n", err)
		return
	}
	respStr, err := json.Marshal(resp)
	if err != nil {
		fmt.Printf("MarshalResponse failed, err:%v\n", err)
		return
	}
	fmt.Printf("%s", respStr)
}

func createEVMContract() {
	// 创建ChainClient
	client := newCMSDKClient(cmSDKYmlFile)
	fmt.Println("====================== 创建EVM合约 ======================")
	byteCodePath, err := filepath.Abs("../contract/ledger_balance.bin")
	if err != nil {
		fmt.Printf("read contract bytecode path failed, err:%v\n", err)
		return
	}
	byteCode, err := ioutil.ReadFile(byteCodePath)
	if err != nil {
		fmt.Printf("ReadContractFile failed, err:%v\n", err)
		return
	}
	payload, err := client.CreateContractCreatePayload(examples.CalcContractName("balance007"),
		"1.0.0", string(byteCode), cmCommonPb.RuntimeType_EVM, []*cmCommonPb.KeyValuePair{})
	if err != nil {
		fmt.Printf("CreateContractCreatePayload failed, err:%v\n", err)
		return
	}
	// 构造EndorsementEntry
	var endorsers []*cmCommonPb.EndorsementEntry
	var entry *cmCommonPb.EndorsementEntry
	for _, orgId := range orgs {
		entry, err = cmSDK.SignPayloadWithPath(fmt.Sprintf(adminKeyPath, orgId), fmt.Sprintf(adminCrtPath, orgId), payload)
		if err != nil {
			fmt.Printf("SignPayloadWithPath failed, err:%v\n", err)
			return
		}
		endorsers = append(endorsers, entry)
	}
	// 发送创建合约请求
	resp, err := client.SendContractManageRequest(payload, endorsers, 10, true)
	if err != nil {
		fmt.Printf("SendContractManageRequest failed, err:%v\n", err)
		return
	}
	respStr, err := json.Marshal(resp)
	if err != nil {
		fmt.Printf("MarshalResponse failed, err:%v\n", err)
		return
	}
	fmt.Printf("%s", respStr)
}

func sendEVMTx() {
	// 创建ChainClient
	client := newCMSDKClient(cmSDKYmlFile)
	// 构造方法名和参数
	abip, err := filepath.Abs(abiPath)
	if err != nil {
		fmt.Printf("abs abi path failed, err:%v\n", err)
		return
	}
	abiJson, err := ioutil.ReadFile(abip)
	if err != nil {
		fmt.Printf("ReadABIPathFile failed, err:%v\n", err)
		return
	}
	myAbi, err := abi.JSON(strings.NewReader(string(abiJson)))
	if err != nil {
		fmt.Printf("abiJSON failed, err:%v\n", err)
		return
	}
	addr := evmutils.BigToAddress(evmutils.FromDecimalString(address))
	dataByte, err := myAbi.Pack("updateBalance", big.NewInt(int64(balance)), addr)
	if err != nil {
		fmt.Printf("abiPack failed, err:%v\n", err)
		return
	}
	dataString := hex.EncodeToString(dataByte)
	method := dataString[0:8]
	pairs := []*cmCommonPb.KeyValuePair{
		{
			Key:   "data",
			Value: []byte(dataString),
		},
	}
	fmt.Println("====================== 发送交易，调用EVM合约 ======================")
	resp, err := client.InvokeContract(examples.CalcContractName("balance007"), method, "", pairs, -1, true)
	if err != nil {
		fmt.Printf("InvokeContract failed, err:%v\n", err)
		return
	}
	respStr, err := json.Marshal(resp)
	if err != nil {
		fmt.Printf("MarshalResponse failed, err:%v\n", err)
		return
	}
	fmt.Printf("%s", respStr)
}

func getTxInfo() {
	// 创建ChainClient
	client := newCMSDKClient(cmSDKYmlFile)
	// 根据高度获取区块
	block, err := client.GetBlockByHeight(uint64(height), false)
	if err != nil {
		fmt.Printf("GetBlockByHeight failed, err:%v\n", err)
		return
	}
	contractInfo, err := chainmaker_tools.GetContractInfoByTxPayload(block.Block.Txs[0])
	if err != nil {
		fmt.Printf("GetContractInfoByTxPayload failed, err:%v\n", err)
		return
	}
	fmt.Printf("=======================查询交易信息=======================\n")
	fmt.Printf("Tx info: \n"+
		"validation code: %d\nblock height: %d\nblock hash: %x\ntxId: %s\ncontract info:%v\n",
		block.Block.Txs[0].Result.Code, height, block.Block.Header.BlockHash, block.Block.Txs[0].Payload.TxId, contractInfo)
}

func verifyTransaction() {
	// 创建RpcProverClient
	conn, client := newRpcProverClient()
	defer conn.Close()

	// 读取配置文件中的交易信息
	cmViper := viper.New()
	cmViper.SetConfigFile(txInfoPath)
	if err := cmViper.ReadInConfig(); err != nil {
		fmt.Printf("read txInfoPath error: %v\n", err)
		return
	}
	txInfo := &TxInfo{}
	if err := cmViper.Unmarshal(txInfo); err != nil {
		fmt.Printf("unmarshal txInfo error: %v\n", err)
		return
	}

	// 构造ContractValidData
	contractData := &api.ContractData{
		Name:   txInfo.ContractData.Name,
		Method: txInfo.ContractData.Method,
		Extra:  nil,
	}
	for _, param := range txInfo.ContractData.Params {
		pair := &api.KVPair{
			Key:   param.Key,
			Value: []byte(param.Value),
		}
		contractData.Params = append(contractData.Params, pair)
	}

	// 如果是调用EVM合约的交易,对Method和Params进行编码
	if runTime == int(cmCommonPb.RuntimeType_EVM) {
		if abiPath == "" {
			fmt.Printf("abiPath should not be empty")
			return
		}
		abs, err := filepath.Abs(abiPath)
		if err != nil {
			abiPath = abs
		}
		codeContractData(contractData, abiPath)
	}
	// 构造TxValidationRequest
	request := &api.TxValidationRequest{
		ChainId:      txInfo.ChainId,
		BlockHeight:  uint64(txInfo.BlockHeight),
		Index:        -1,
		TxKey:        txInfo.TxKey,
		ContractData: contractData,
		Extra:        nil,
	}
	// 交易有效性证明
	fmt.Println("====================== 交易有效性验证 ======================")
	response, err := client.ValidTransaction(context.Background(), request)
	if err != nil {
		fmt.Printf("invoke grpc failed, err:%v", err)
	}
	responseStr, err := json.Marshal(response)
	if err != nil {
		fmt.Printf("MarshalResponse failed, err:%v", err)
	}
	fmt.Printf("%s", responseStr)
}

func newCMSDKClient(ymlFile string) *cmSDK.ChainClient {
	client, err := cmSDK.NewChainClient(
		cmSDK.WithConfPath(ymlFile),
	)
	if err != nil {
		fmt.Printf("NewChainClient failed, err:%v\n", err)
		panic("NewChainClient failed")
	}
	return client
}

func newRpcConnection() (*grpc.ClientConn, error) {
	// 双向认证
	if enableCa {
		//从证书相关文件中读取和解析信息，得到证书公钥、密钥对
		cert, err := tls.LoadX509KeyPair(tlsClientCert, tlsClientKey)
		if err != nil {
			fmt.Printf("grpc client read cert and key file failed, err:%v\n", err)
			return nil, nil
		}
		// 创建一个新的、空的 CertPool
		certPool := x509.NewCertPool()
		ca, err := ioutil.ReadFile(tlsCa)
		if err != nil {
			fmt.Printf("grpc client read ca file failed, err:%v\n", err)
			return nil, nil
		}
		//尝试解析所传入的 PEM 编码的证书。如果解析成功会将其加到 CertPool 中，便于后面的使用
		certPool.AppendCertsFromPEM(ca)
		//构建基于 TLS 的 TransportCredentials 选项
		cred := credentials.NewTLS(&tls.Config{ // nolint
			//设置证书链，允许包含一个或多个
			Certificates: []tls.Certificate{cert},
			//要求必须校验客户端的证书。可以根据实际情况选用以下参数
			ServerName: "localhost",
			RootCAs:    certPool,
		})
		return grpc.Dial(grpcAP, grpc.WithTransportCredentials(cred))
	}
	// 单向认证
	if !enableCa && enableTls {
		// 这里Common Name 为刚刚生成自签证书所填写的
		cred, err := credentials.NewClientTLSFromFile(tlsCa, "localhost")
		if err != nil {
			fmt.Printf("grpc client read cert and key file failed, err:%v\n", err)
			return nil, nil
		}
		return grpc.Dial(grpcAP, grpc.WithTransportCredentials(cred))
	}
	// 无认证
	return grpc.Dial(grpcAP, grpc.WithInsecure())
}

func newRpcProverClient() (*grpc.ClientConn, api.RpcProverClient) {
	conn, err := newRpcConnection()
	if err != nil {
		fmt.Printf("new grpc connection failed, err:%v", err)
	}
	client := api.NewRpcProverClient(conn)
	return conn, client
}

func newRpcForwarderClient() (*grpc.ClientConn, api.RpcForwarderClient) {
	conn, err := newRpcConnection()
	if err != nil {
		fmt.Printf("new grpc connection failed, err:%v", err)
	}
	client := api.NewRpcForwarderClient(conn)
	return conn, client
}

func codeContractData(contractData *api.ContractData, abiPath string) {
	if contractData == nil || contractData.Method == "" || contractData.Params == nil {
		fmt.Printf("ContractValidData should not be nill\n")
		return
	}
	// 编码method和params
	abiJson, err := ioutil.ReadFile(abiPath)
	if err != nil {
		fmt.Printf("Read abiPath failed, err:%s\n", err.Error())
		return
	}
	myAbi, err := abi.JSON(strings.NewReader(string(abiJson)))
	if err != nil {
		fmt.Printf("abiJSON failed, err:%s\n", err.Error())
		return
	}
	var (
		method string
		add    string
		bal    string
	)
	for _, pair := range contractData.Params {
		if pair.Key == "address" {
			add = string(pair.Value)
		}
		if pair.Key == "balance" {
			bal = string(pair.Value)
		}
	}
	addr := evmutils.BigToAddress(evmutils.FromDecimalString(add))
	balInt64, err := strconv.ParseInt(bal, 10, 64)
	if err != nil {
		fmt.Printf("ParseInt balance failed, err:%s\n", err.Error())
		return
	}
	dataByte, err := myAbi.Pack(contractData.Method, big.NewInt(balInt64), addr)
	if err != nil {
		fmt.Printf("Params coding failed, err:%s\n", err.Error())
		return
	}
	dataString := hex.EncodeToString(dataByte)
	method = dataString[0:8]
	pair := &api.KVPair{
		Key:   "data",
		Value: []byte(dataString),
	}
	parameters := make([]*api.KVPair, 0)
	parameters = append(parameters, pair)
	// 更新contractData
	contractData.Name = examples.CalcContractName(contractData.Name)
	contractData.Method = method
	contractData.Params = parameters
}

// Fabric Func
func queryBalance() {
	sdk, err := fabsdk.New(config.FromFile(fabSDKYmlFile))
	if err != nil {
		fmt.Printf("failed to create fabric sdk: %s\n", err)
		return
	}
	ccp := sdk.ChannelContext(channelId, fabsdk.WithUser(user))
	cc, err := channel.New(ccp)
	if err != nil {
		fmt.Printf("failed to create channel client: %s\n", err)
		return
	}
	// new channel request for query
	req := channel.Request{
		ChaincodeID: "mycc",
		Fcn:         "query",
		Args:        packArgs([]string{e2eFrom}),
	}
	// send request and handle response
	reqPeers := channel.WithTargetEndpoints(peer0org1)
	resp, err := cc.Query(req, reqPeers)
	if err != nil {
		fmt.Printf("query chaincode error, err: %s \n", err)
		return
	}
	fmt.Printf("Querey chaincode response:\n "+
		"TxId: %s\n Balance: %s\n",
		resp.TransactionID,
		resp.Payload)
}

func invokeSendTx() {
	sdk, err := fabsdk.New(config.FromFile(fabSDKYmlFile))
	if err != nil {
		fmt.Printf("failed to create fabric sdk: %s\n", err)
		return
	}
	ccp := sdk.ChannelContext(channelId, fabsdk.WithUser(user))
	cc, err := channel.New(ccp)
	if err != nil {
		fmt.Printf("failed to create channel client: %s\n", err)
		return
	}
	// new channel request for invoke
	req := channel.Request{
		ChaincodeID: "mycc",
		Fcn:         "invoke",
		Args:        packArgs([]string{e2eFrom, e2eTo, e2eVal}),
	}
	// send request and handle response
	reqPeers := channel.WithTargetEndpoints(peer0org1, peer0org2)
	resp, err := cc.Execute(req, reqPeers)
	if err != nil {
		fmt.Printf("invoke chaincode error, err: %s \n", err)
		return
	}
	fmt.Printf("Invoke chaincode response:\n "+
		"TxId: %s\n validate: %d\n chaincode status: %d\n",
		resp.TransactionID,
		resp.TxValidationCode,
		resp.ChaincodeStatus)
}

func subscribeBlock() {
	sdk, err := fabsdk.New(config.FromFile(fabSDKYmlFile))
	if err != nil {
		fmt.Printf("failed to create fabric sdk: %s\n", err)
		return
	}
	cp := sdk.ChannelContext(channelId, fabsdk.WithUser(user))
	ec, err := event.New(
		cp,
		event.WithBlockEvents(),         // 添加该option表明订阅完整的区块信息，如果没有会是不完整的区块信息(filtered)
		event.WithSeekType(seek.Oldest)) // Oldest、Newest和FromBlock，分别代表从第1个区块、最后一个区块、指定区块号开始获取事件
	//event.WithBlockNum(1) // 指定区块高度，只有WithSeekType(FromBlock)时才生效
	if err != nil {
		fmt.Printf("Create event client error: %v", err)
		return
	}
	// Register monitor block event
	beReg, beCh, err := ec.RegisterBlockEvent()
	if err != nil {
		fmt.Printf("Register block event error: %v", err)
		return
	}
	defer ec.Unregister(beReg)
	fmt.Println("Registered block event")

	var wg sync.WaitGroup
	wg.Add(1)
	// Receive block event
	go func() {
		defer wg.Done()
		for e := range beCh {
			fmt.Printf("Receive block event:\n"+
				"SourceURL: %v\nNumber: %v\nDataHash: %v\nBlockHash: %v\nPreviousHash: %v\n",
				e.SourceURL,
				e.Block.Header.Number,
				hex.EncodeToString(e.Block.Header.DataHash),
				hex.EncodeToString(blockHeaderHash(e.Block.Header)),
				hex.EncodeToString(e.Block.Header.PreviousHash))
		}
	}()
	fmt.Println("waitting")
	wg.Wait()
	fmt.Println("finish")
}

func queryBlock() {
	sdk, err := fabsdk.New(config.FromFile(fabSDKYmlFile))
	if err != nil {
		fmt.Printf("failed to create fabric sdk: %s\n", err.Error())
		return
	}
	ccp := sdk.ChannelContext(channelId, fabsdk.WithUser(user))
	lc, err := ledger.New(ccp)
	if err != nil {
		fmt.Printf("failed to create ledger client: %s\n", err.Error())
		return
	}
	peers := []string{peer0org1, peer1org1, peer0org2, peer1org2}
	reqPeers := ledger.WithTargetEndpoints(peers...)
	minTarget := ledger.WithMinTargets(1)
	maxTarget := ledger.WithMaxTargets(4)
	block, err := lc.QueryBlock(uint64(number), reqPeers, minTarget, maxTarget)
	if err != nil {
		fmt.Printf("failed to query block: %s\n", err.Error())
		return
	}
	fmt.Printf("Block info:\n "+
		"Number: %v\nDataHash: %v\nBlockHash: %v\nPreviousHash: %v\n",
		block.Header.GetNumber(),
		hex.EncodeToString(block.Header.DataHash),
		hex.EncodeToString(blockHeaderHash(block.Header)),
		hex.EncodeToString(block.Header.PreviousHash))
}

func queryTransaction() {
	sdk, err := fabsdk.New(config.FromFile(fabSDKYmlFile))
	if err != nil {
		fmt.Printf("failed to create fabric sdk: %s\n", err.Error())
		return
	}
	ccp := sdk.ChannelContext(channelId, fabsdk.WithUser(user))
	lc, err := ledger.New(ccp)
	if err != nil {
		fmt.Printf("failed to create ledger client: %s\n", err.Error())
		return
	}
	id := fab.TransactionID(txId)
	reqPeers := ledger.WithTargetEndpoints(peer0org1)
	tx, err := lc.QueryTransaction(id, reqPeers)
	if err != nil {
		fmt.Printf("failed to query transaction: %s\n", err.Error())
		return
	}
	// get channelId from transaction
	chId, err := getTransactionChannelId(tx.TransactionEnvelope)
	if err != nil {
		fmt.Printf("parse transaction to get channelId failed, err: %s\n", err.Error())
		return
	}
	// get contract data from transaction
	contractName, args, err := getTransactionContractData(tx.TransactionEnvelope)
	if err != nil {
		fmt.Printf("parse transaction to get contract data failed, err: %s\n", err.Error())
		return
	}
	fmt.Printf("Transaction info:\n "+
		"ValidationCode: %v\nChannelId: %s\nContractname: %s\nArgs: %s\n",
		tx.ValidationCode,
		chId,
		contractName,
		args)
}

func queryHeight() {
	sdk, err := fabsdk.New(config.FromFile(fabSDKYmlFile))
	if err != nil {
		fmt.Printf("failed to create fabric sdk: %s\n", err.Error())
		return
	}
	ccp := sdk.ChannelContext(channelId, fabsdk.WithUser(user))
	lc, err := ledger.New(ccp)
	if err != nil {
		fmt.Printf("failed to create ledger client: %s\n", err.Error())
		return
	}
	reqPeers := ledger.WithTargetEndpoints(peer0org2)
	resp, err := lc.QueryInfo(reqPeers)
	if err != nil {
		fmt.Printf("failed to query height: %s\n", err.Error())
		return
	}
	fmt.Printf("Height info: \n"+
		"Height:%d\nCurrentBlockHash:%x\nPreHash:%x\n",
		resp.BCI.Height, resp.BCI.CurrentBlockHash, resp.BCI.PreviousBlockHash)
}

func subscribeCCEvent() {
	sdk, err := fabsdk.New(config.FromFile(fabSDKYmlFile))
	if err != nil {
		fmt.Printf("failed to create fabric sdk: %s\n", err)
		return
	}
	cp := sdk.ChannelContext(channelId, fabsdk.WithUser(user))
	ec, err := event.New(
		cp,
		event.WithBlockEvents(),         // 添加该option表明订阅完整的区块信息，如果没有会是不完整的区块信息(filtered)
		event.WithSeekType(seek.Oldest)) // Oldest、Newest和FromBlock，分别代表从第1个区块、最后一个区块、指定区块号开始获取事件
	//event.WithBlockNum(1) // 指定区块高度，只有WithSeekType(FromBlock)时才生效
	if err != nil {
		fmt.Printf("Create event client error: %v", err)
		return
	}
	// Register monitor cc event
	ceReg, ceCh, err := ec.RegisterChaincodeEvent("mycc", ".*")
	if err != nil {
		fmt.Printf("Register block event error: %v", err)
		return
	}
	defer ec.Unregister(ceReg)
	fmt.Println("Registered cc event")

	var wg sync.WaitGroup
	wg.Add(1)
	// Receive block event
	go func() {
		defer wg.Done()
		for e := range ceCh {
			fmt.Printf("Receive cc event:\n"+
				"SourceURL: %v\nBlockNumber: %v\nEventName: %v\nCCID: %v\nPayload: %s\n",
				e.SourceURL, e.BlockNumber, e.EventName, e.ChaincodeID, e.Payload)
		}
	}()

	fmt.Println("waitting")
	wg.Wait()
	fmt.Println("finish")
}

func verifyFabricTransaction() {
	// 构造Client
	conn, err := grpc.Dial(grpcAP, grpc.WithInsecure())
	if err != nil {
		fmt.Printf("grpcDial failed, err:%v\n", err)
		return
	}
	defer conn.Close()
	client := api.NewRpcProverClient(conn)

	// 读取配置文件中的交易信息
	cmViper := viper.New()
	cmViper.SetConfigFile(txInfoPath)
	if err = cmViper.ReadInConfig(); err != nil {
		fmt.Printf("read txInfoPath error: %v\n", err)
		return
	}
	txInfo := &TxInfo{}
	if err = cmViper.Unmarshal(txInfo); err != nil {
		fmt.Printf("unmarshal txInfo error: %v\n", err)
		return
	}

	// 构造ContractValidData
	contractData := &api.ContractData{
		Name:   txInfo.ContractData.Name,
		Method: txInfo.ContractData.Method,
		Extra:  nil,
	}

	for _, param := range txInfo.ContractData.Params {
		pair := &api.KVPair{
			Key:   param.Key,
			Value: []byte(param.Value),
		}
		contractData.Params = append(contractData.Params, pair)
	}
	// 构造TxValidationRequest
	txValidationInfo := &api.TxValidationRequest{
		ChainId:      txInfo.ChainId,
		BlockHeight:  uint64(txInfo.BlockHeight),
		Index:        -1,
		TxKey:        txInfo.TxKey,
		ContractData: contractData,
		Extra:        nil,
	}
	// 交易有效性证明
	fmt.Println("====================== 交易有效性验证 ======================")
	response, _ := client.ValidTransaction(context.Background(), txValidationInfo)
	responseStr, err := json.Marshal(response)
	if err != nil {
		fmt.Printf("MarshalResponse failed, err:%v", err)
	}
	fmt.Printf("%s", responseStr)
}

func packArgs(paras []string) [][]byte {
	var args [][]byte
	for _, k := range paras {
		args = append(args, []byte(k))
	}
	return args
}

// calc block hash for fabric
type asn1Header struct {
	Number       *big.Int
	PreviousHash []byte
	DataHash     []byte
}

func blockHeaderBytes(b *fabCommonPb.BlockHeader) []byte {
	header := asn1Header{
		PreviousHash: b.PreviousHash,
		DataHash:     b.DataHash,
		Number:       new(big.Int).SetUint64(b.Number),
	}
	result, err := asn1.Marshal(header)
	if err != nil {
		// Errors should only arise for types which cannot be encoded, since the
		// BlockHeader type is known a-priori to contain only encodable types, an
		// error here is fatal and should not be propagated
		panic(err)
	}
	return result
}

func blockHeaderHash(b *fabCommonPb.BlockHeader) []byte {
	sum := sha256.Sum256(blockHeaderBytes(b))
	return sum[:]
}

// parse transaction for fabric to get channelId
func getTransactionChannelId(envelope *fabCommonPb.Envelope) (string, error) {
	if envelope == nil {
		return "", errors.New("envelope should not be nil")
	}

	// Envelope -> Payload
	payload := &fabCommonPb.Payload{}
	err := proto.Unmarshal(envelope.Payload, payload)
	if err != nil {
		return "", err
	}

	// Payload.Header.ChannelHeader -> ChannelHeader
	channelHeader := &fabCommonPb.ChannelHeader{}
	err = proto.Unmarshal(payload.Header.ChannelHeader, channelHeader)
	if err != nil {
		return "", err
	}

	return channelHeader.ChannelId, nil
}

// parse transaction for fabric to get contract name and args
func getTransactionContractData(envelope *fabCommonPb.Envelope) (string, [][]byte, error) {
	var (
		contractName string
		args         [][]byte
	)

	if envelope == nil {
		return "", nil, errors.New("envelope should not be nil")
	}

	// Envelope -> Payload
	payload := &fabCommonPb.Payload{}
	err := proto.Unmarshal(envelope.Payload, payload)
	if err != nil {
		return "", nil, err
	}

	// Payload.Data -> Transaction
	transaction := &fabPeerPb.Transaction{}
	err = proto.Unmarshal(payload.Data, transaction)
	if err != nil {
		return "", nil, err
	}

	// TransactionAction.Payload -> ChaincodeActionPayload
	chaincodeActionPayload := &fabPeerPb.ChaincodeActionPayload{}
	err = proto.Unmarshal(transaction.Actions[0].Payload, chaincodeActionPayload)
	if err != nil {
		return "", nil, err
	}

	// ChaincodeActionPayload.ChaincodeProposalPayload -> ChaincodeProposalPayload
	chaincodeProposalPayload := &fabPeerPb.ChaincodeProposalPayload{}
	err = proto.Unmarshal(chaincodeActionPayload.ChaincodeProposalPayload, chaincodeProposalPayload)
	if err != nil {
		return "", nil, err
	}

	// ChaincodeProposalPayload.Input -> ChaincodeInvocationSpec
	chaincodeInvocationSpec := &fabPeerPb.ChaincodeInvocationSpec{}
	err = proto.Unmarshal(chaincodeProposalPayload.Input, chaincodeInvocationSpec)
	if err != nil {
		return "", nil, err
	}

	// ChaincodeInvocationSpec -> ChaincodeSpec
	chaincodeSpec := chaincodeInvocationSpec.ChaincodeSpec

	// get contract name and args
	contractName = chaincodeSpec.ChaincodeId.Name
	args = chaincodeSpec.Input.Args

	return contractName, args, nil
}
