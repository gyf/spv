/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package fabric_common

import (
	"chainmaker.org/chainmaker/spv/v2/common"
	"chainmaker.org/chainmaker/spv/v2/utils/fabric_tools"
	fabCommonPb "github.com/hyperledger/fabric-protos-go/common"
)

// FabBlock packages Fabric's block
type FabBlock struct {
	ChainId string
	Block   *fabCommonPb.Block
}

// NewFabBlock creates FabBlock
func NewFabBlock(chainId string, block *fabCommonPb.Block) *FabBlock {
	return &FabBlock{
		ChainId: chainId,
		Block:   block,
	}
}

// GetBlockHeader returns block header
func (fb *FabBlock) GetBlockHeader() common.Header {
	return &FabBlockHeader{
		ChainId:     fb.ChainId,
		BlockHeader: fb.Block.Header,
	}
}

// GetTransaction returns transactions in the block
func (fb *FabBlock) GetTransaction() []common.Transactioner {
	data := fb.Block.Data.Data
	txs := make([]common.Transactioner, len(data))
	for i, val := range data {
		txs[i] = &FabTransaction{
			Transaction:    fabric_tools.GetEnvelopeByBytes(val),
			ValidationCode: fabric_tools.GetValidationCodeByMetadata(i, fb.Block.Metadata),
		}
	}
	return txs
}

// GetExtraData returns extra data in the block
func (fb *FabBlock) GetExtraData() interface{} {
	// FabBlock does not contain extra data, such as re_set
	return nil
}

// GetChainId returns chainId
func (fb *FabBlock) GetChainId() string {
	return fb.ChainId
}

// GetPreHash returns the hash value of previous block
func (fb *FabBlock) GetPreHash() []byte {
	return fb.Block.Header.PreviousHash
}

// GetTxRoot returns the root hash of transaction tree
func (fb *FabBlock) GetTxRoot() []byte {
	return fb.Block.Header.DataHash
}

// GetHeight returns block height
func (fb *FabBlock) GetHeight() uint64 {
	return fb.Block.Header.Number
}

// GetBlockHash returns block hash
func (fb *FabBlock) GetBlockHash() []byte {
	return fabric_tools.GetBlockHash(fb.Block.Header)
}

// FabBlockHeader packages Fabric's block header
type FabBlockHeader struct {
	ChainId     string
	BlockHeader *fabCommonPb.BlockHeader
}

// GetChainId returns chainId
func (fbh *FabBlockHeader) GetChainId() string {
	return fbh.ChainId
}

// GetPreHash returns the hash value of previous block
func (fbh *FabBlockHeader) GetPreHash() []byte {
	return fbh.BlockHeader.PreviousHash
}

// GetTxRoot returns the root hash of transactions
func (fbh *FabBlockHeader) GetTxRoot() []byte {
	return fbh.BlockHeader.DataHash
}

// GetHeight returns block height
func (fbh *FabBlockHeader) GetHeight() uint64 {
	return fbh.BlockHeader.Number
}

// GetBlockHash returns block hash
func (fbh *FabBlockHeader) GetBlockHash() []byte {
	return fabric_tools.GetBlockHash(fbh.BlockHeader)
}
