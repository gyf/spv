/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package fabric_common

import (
	"errors"

	"chainmaker.org/chainmaker/spv/v2/utils/fabric_tools"
	"github.com/golang/protobuf/proto" // nolint
	fabCommonPb "github.com/hyperledger/fabric-protos-go/common"
)

// FabTransaction packages Fabric's Transaction
type FabTransaction struct {
	Transaction    *fabCommonPb.Envelope
	ValidationCode int32 // An indication of whether the transaction was validated or invalidated by committing peer
}

// NewFabTransaction creates FabTransaction
func NewFabTransaction(tx *fabCommonPb.Envelope, code int32) *FabTransaction {
	return &FabTransaction{
		Transaction:    tx,
		ValidationCode: code,
	}
}

// GetStatusCode returns the transaction status code
func (ft *FabTransaction) GetStatusCode() int32 {
	return ft.ValidationCode
}

// GetTransactionHash returns transaction hash
func (ft *FabTransaction) GetTransactionHash() ([]byte, error) {
	data, err := proto.Marshal(ft.Transaction)
	if err != nil {
		return nil, err
	}
	return data, nil
}

// GetContractName returns the contract name of transaction
func (ft *FabTransaction) GetContractName() (string, error) {
	name, _, err := fabric_tools.GetTransactionContractData(ft.Transaction)
	if err != nil {
		return "", err
	}
	return name, nil
}

// GetMethod returns the method in contract method of transaction
func (ft *FabTransaction) GetMethod() (string, error) {
	_, args, err := fabric_tools.GetTransactionContractData(ft.Transaction)
	if err != nil {
		return "", err
	}
	return string(args[0]), nil
}

// GetParams returns parameters of transaction
func (ft *FabTransaction) GetParams() ([]interface{}, error) {
	_, args, err := fabric_tools.GetTransactionContractData(ft.Transaction)
	if err != nil {
		return nil, err
	}
	var parameters []interface{}
	for i := 1; i < len(args); i++ {
		arg := args[i]
		parameters = append(parameters, arg)
	}
	return parameters, nil
}

// GetExtraData returns extra data of transaction.
func (ft *FabTransaction) GetExtraData() (interface{}, error) {
	// FabTransaction does not contain extra data, such as re_set
	return nil, errors.New("no extra data in FabTransaction")
}
