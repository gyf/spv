/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package chainmaker_common

import (
	"testing"

	cmCommonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/spv/v2/conf"
	"chainmaker.org/chainmaker/spv/v2/pb/protogo"
	"github.com/stretchr/testify/require"
)

func TestCMTransaction_Func(t *testing.T) {
	conf.RemoteChainConfigs["chain1"] = &protogo.RemoteConfig{
		ChainId:  "chain1",
		HashType: "SHA256",
	}
	tran := &CMTransaction{
		Transaction: &cmCommonPb.Transaction{
			Payload: &cmCommonPb.Payload{
				ChainId:      "chain1",
				TxId:         "123",
				ContractName: "contractName",
				Method:       "method",
				Parameters: []*cmCommonPb.KeyValuePair{
					{
						Key:   "key",
						Value: []byte("value"),
					},
				},
			},
			Result: &cmCommonPb.Result{
				Code:           cmCommonPb.TxStatusCode_SUCCESS,
				ContractResult: &cmCommonPb.ContractResult{},
			},
		},
		RWSet: &cmCommonPb.TxRWSet{
			TxId: "123",
		},
	}

	code := tran.GetStatusCode()
	require.Equal(t, code, int32(cmCommonPb.TxStatusCode_SUCCESS))

	hash, err := tran.GetTransactionHash()
	require.NotNil(t, hash)
	require.Nil(t, err)

	cn, err := tran.GetContractName()
	require.Equal(t, cn, "contractName")
	require.Nil(t, err)

	method, err := tran.GetMethod()
	require.Equal(t, method, "method")
	require.Nil(t, err)

	params, err := tran.GetParams()
	require.NotNil(t, params)
	require.Nil(t, err)

	extra, err := tran.GetExtraData()
	require.NotNil(t, extra)
	require.Nil(t, err)
}
