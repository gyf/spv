/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package chainmaker_common

import (
	"testing"

	cmCommonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	"github.com/stretchr/testify/require"
)

func TestCMBlock_Func(t *testing.T) {
	block := CMBlock{
		Block: &cmCommonPb.Block{
			Header: &cmCommonPb.BlockHeader{
				ChainId:      "chain1",
				PreBlockHash: []byte("preHash"),
				TxRoot:       []byte("txRoot"),
				BlockHeight:  uint64(1),
				BlockHash:    []byte("blockHash"),
			},
			Txs: []*cmCommonPb.Transaction{
				{Payload: &cmCommonPb.Payload{
					ChainId: "chain1",
					TxId:    "123",
				}},
			},
		},
		RWSetList: []*cmCommonPb.TxRWSet{
			{TxId: "123"},
		},
	}
	header := block.GetBlockHeader()
	require.NotNil(t, header)

	txs := block.GetTransaction()
	require.NotNil(t, txs)

	extra := block.GetExtraData()
	require.NotNil(t, extra)

	chainId := block.GetChainId()
	require.Equal(t, chainId, "chain1")

	preHash := block.GetPreHash()
	require.Equal(t, string(preHash), "preHash")

	txRoot := block.GetTxRoot()
	require.Equal(t, string(txRoot), "txRoot")

	height := block.GetHeight()
	require.Equal(t, height, uint64(1))

	blockHash := block.GetBlockHash()
	require.Equal(t, string(blockHash), "blockHash")
}

func TestCMBlockHeader_Func(t *testing.T) {
	header := &CMBlockHeader{
		BlockHeader: &cmCommonPb.BlockHeader{
			ChainId:      "chain1",
			PreBlockHash: []byte("preHash"),
			TxRoot:       []byte("txRoot"),
			BlockHeight:  uint64(1),
			BlockHash:    []byte("blockHash"),
		},
	}
	chainId := header.GetChainId()
	require.Equal(t, chainId, "chain1")

	preHash := header.GetPreHash()
	require.Equal(t, string(preHash), "preHash")

	txRoot := header.GetTxRoot()
	require.Equal(t, string(txRoot), "txRoot")

	height := header.GetHeight()
	require.Equal(t, height, uint64(1))

	blockHash := header.GetBlockHash()
	require.Equal(t, string(blockHash), "blockHash")
}
