/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package fabric_coder

import (
	"testing"

	"chainmaker.org/chainmaker/spv/v2/common/fabric_common"
	"chainmaker.org/chainmaker/spv/v2/utils/fabric_tools"
	"github.com/stretchr/testify/require"
)

func TestFabCoder_Func(t *testing.T) {
	fabCoder := NewFabricCoder()
	block := fabric_tools.NewBlock()
	fabHeader := &fabric_common.FabBlockHeader{
		ChainId:     "mychannel",
		BlockHeader: block.Header,
	}
	fabBlock := &fabric_common.FabBlock{
		ChainId: "mychannel",
		Block:   block,
	}

	blockData, err := fabCoder.GenerateBlockData(fabHeader)
	require.NotNil(t, blockData)
	require.Nil(t, err)

	header, err := fabCoder.DeserializeBlockHeader(blockData.SerializedHeader)
	require.NotNil(t, header)
	require.Nil(t, err)

	td, err := fabCoder.GenerateTransactionData(fabBlock)
	require.NotNil(t, td)
	require.Nil(t, err)

	tx, err := fabCoder.DeserializeTransaction(fabBlock.Block.Data.Data[0], nil)
	require.NotNil(t, tx)
	require.Nil(t, err)
}
