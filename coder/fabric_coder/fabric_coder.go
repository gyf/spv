/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package fabric_coder

import (
	"errors"
	"fmt"
	"reflect"

	"chainmaker.org/chainmaker/spv/v2/common"
	"chainmaker.org/chainmaker/spv/v2/common/fabric_common"
	"chainmaker.org/chainmaker/spv/v2/pb/protogo"
	"chainmaker.org/chainmaker/spv/v2/storage"
	"chainmaker.org/chainmaker/spv/v2/utils/fabric_tools"
	"github.com/gogo/protobuf/proto"
	fabCommonPb "github.com/hyperledger/fabric-protos-go/common"
)

// FabricCoder is the implementation of Coder interface for Fabric
type FabricCoder struct{}

// NewFabricCoder creates FabricCoder
func NewFabricCoder() *FabricCoder {
	return &FabricCoder{}
}

// GenerateBlockData generates transaction data, contains block hash and block header bytes
func (fc *FabricCoder) GenerateBlockData(header common.Header) (*storage.BlockData, error) {
	if header == nil {
		return nil, errors.New("header should not be nil")
	}
	fbHeader, ok := header.(*fabric_common.FabBlockHeader)
	if !ok {
		return nil, fmt.Errorf("assert FabBlockHeader failed, get type:{%s}, want type:{%s}",
			reflect.TypeOf(header), reflect.TypeOf(fabric_common.FabBlockHeader{}))
	}
	fabricHeader := &protogo.FabricBlockHeader{
		ChainId:      fbHeader.ChainId,
		Number:       fbHeader.BlockHeader.Number,
		PreviousHash: fbHeader.BlockHeader.PreviousHash,
		DataHash:     fbHeader.BlockHeader.DataHash,
	}
	fabricHeaderBz, err := proto.Marshal(fabricHeader)
	if err != nil {
		return nil, err
	}
	blockData := storage.NewBlockData(header.GetBlockHash(), fabricHeaderBz)
	return blockData, nil
}

// DeserializeBlockHeader deserializes binary bytes to Fabric's block header
func (fc *FabricCoder) DeserializeBlockHeader(headerBz []byte) (common.Header, error) {
	if headerBz == nil {
		return nil, errors.New("headerBz should not be nil")
	}
	fabricHeader := &protogo.FabricBlockHeader{}
	err := proto.Unmarshal(headerBz, fabricHeader)
	if err != nil {
		return nil, err
	}
	return &fabric_common.FabBlockHeader{
		ChainId: fabricHeader.ChainId,
		BlockHeader: &fabCommonPb.BlockHeader{
			Number:       fabricHeader.Number,
			PreviousHash: fabricHeader.PreviousHash,
			DataHash:     fabricHeader.DataHash,
		},
	}, nil
}

// GenerateTransactionData generates transaction data, only contains tx hash map with the txs in block
func (fc *FabricCoder) GenerateTransactionData(blocker common.Blocker) (*storage.TransactionData, error) {
	if blocker == nil {
		return nil, errors.New("blocker should not be nil")
	}
	fabBlock, ok := blocker.(*fabric_common.FabBlock)
	if !ok {
		return nil, fmt.Errorf("assert FabBlock failed, get type:{%s}, want type:{%s}",
			reflect.TypeOf(blocker), reflect.TypeOf(fabric_common.FabBlock{}))
	}
	data := fabBlock.Block.Data.Data
	txHashMap := make(map[string][]byte, len(data))
	for _, txHash := range data {
		tx := fabric_tools.GetEnvelopeByBytes(txHash)
		txId, err := fabric_tools.GetTransactionId(tx)
		if err != nil {
			return nil, err
		}
		txHashMap[txId] = txHash
	}
	td := storage.NewTransactionData()
	td.TxHashMap = txHashMap
	return td, nil
}

// DeserializeTransaction deserializes the binary bytes of transaction and extra data to transaction
func (fc *FabricCoder) DeserializeTransaction(transactionBz []byte, extraDataBz []byte) (
	common.Transactioner, error) {
	if transactionBz == nil {
		return nil, errors.New("transactionBz is nil")
	}
	ev := fabric_tools.GetEnvelopeByBytes(transactionBz)
	fabTx := &fabric_common.FabTransaction{Transaction: ev}
	return fabTx, nil
}
