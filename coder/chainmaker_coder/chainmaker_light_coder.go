/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package chainmaker_coder

import (
	"errors"

	"chainmaker.org/chainmaker/spv/v2/common"
	"chainmaker.org/chainmaker/spv/v2/storage"
)

// CMLightCoder is the implementation of Coder interface for ChainMaker
type CMLightCoder struct {
	cmCoder *cmCoder
}

// NewCMLightCoder creates CMLightCoder
func NewCMLightCoder() *CMLightCoder {
	return &CMLightCoder{
		cmCoder: newCMCoder(),
	}
}

// GenerateBlockData generates transaction data, contains block hash and block header bytes
func (cms *CMLightCoder) GenerateBlockData(header common.Header) (*storage.BlockData, error) {
	if header == nil {
		return nil, errors.New("header should not be nil")
	}
	return cms.cmCoder.generateBlockData(header)
}

// DeserializeBlockHeader deserializes binary bytes to ChainMaker's block header
func (cms *CMLightCoder) DeserializeBlockHeader(headerBz []byte) (common.Header, error) {
	if headerBz == nil {
		return nil, errors.New("headerBz should not be nil")
	}
	return cms.cmCoder.deserializeBlockHeader(headerBz)
}

// GenerateTransactionData generates transaction data, contains txHashMap, txBytesMap and txRWSetMap
func (cms *CMLightCoder) GenerateTransactionData(blocker common.Blocker) (*storage.TransactionData, error) {
	if blocker == nil {
		return nil, errors.New("blocker should not be nil")
	}
	return cms.cmCoder.generateTransactionData(blocker, true, true, true)
}

// DeserializeTransaction deserializes the binary bytes of transaction and extra data(rw_set) to transaction
func (cms *CMLightCoder) DeserializeTransaction(transactionBz []byte, extraDataBz []byte) (
	common.Transactioner, error) {
	if transactionBz == nil {
		return nil, errors.New("transactionBz should not be nil")
	}
	return cms.cmCoder.deserializeTransaction(transactionBz, extraDataBz)
}
