/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package chainmaker_coder

import (
	"testing"

	cmCommonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/spv/v2/common/chainmaker_common"
	"chainmaker.org/chainmaker/spv/v2/conf"
	"chainmaker.org/chainmaker/spv/v2/pb/protogo"
	"github.com/stretchr/testify/require"
)

func TestCMLightCoder_Func(t *testing.T) {
	conf.RemoteChainConfigs["chain1"] = &protogo.RemoteConfig{
		ChainId:  "chain1",
		HashType: "SHA256",
	}
	cms := NewCMLightCoder()
	blockData, err := cms.GenerateBlockData(&chainmaker_common.CMBlockHeader{
		BlockHeader: &cmCommonPb.BlockHeader{},
	})
	require.NotNil(t, blockData)
	require.Nil(t, err)

	header, err := cms.DeserializeBlockHeader(blockData.SerializedHeader)
	require.NotNil(t, header)
	require.Nil(t, err)

	transaction := &cmCommonPb.Transaction{
		Payload: &cmCommonPb.Payload{
			ChainId: "chain1",
			TxId:    "123",
		},
		Result: &cmCommonPb.Result{
			ContractResult: &cmCommonPb.ContractResult{},
		},
	}
	rwSet := &cmCommonPb.TxRWSet{
		TxId: "123",
	}
	td, err := cms.GenerateTransactionData(&chainmaker_common.CMBlock{
		Block: &cmCommonPb.Block{
			Txs: []*cmCommonPb.Transaction{
				transaction,
			},
		},
		RWSetList: []*cmCommonPb.TxRWSet{
			rwSet,
		},
	})
	require.Nil(t, err)
	require.Equal(t, 1, len(td.TxHashMap))
	require.Equal(t, 1, len(td.TxBytesMap))
	require.Equal(t, 1, len(td.ExtraDataMap))
}
