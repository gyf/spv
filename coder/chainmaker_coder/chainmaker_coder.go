/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package chainmaker_coder

import (
	"fmt"
	"reflect"

	cmCommonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/spv/v2/common"
	"chainmaker.org/chainmaker/spv/v2/common/chainmaker_common"
	"chainmaker.org/chainmaker/spv/v2/storage"
	"chainmaker.org/chainmaker/spv/v2/utils/chainmaker_tools"
	"github.com/gogo/protobuf/proto"
)

// cmCoder is the implementation of Coder interface for ChainMaker
type cmCoder struct{}

// newCMCoder creates cmCoder
func newCMCoder() *cmCoder {
	return &cmCoder{}
}

// generateBlockData generates transaction data, contains block hash and block header bytes
func (cmc *cmCoder) generateBlockData(header common.Header) (*storage.BlockData, error) {
	cmBlockHeader, ok := header.(*chainmaker_common.CMBlockHeader)
	if !ok {
		return nil, fmt.Errorf("assert CMBlockHeader failed, get type:{%s}, want type:{%s}",
			reflect.TypeOf(header), reflect.TypeOf(chainmaker_common.CMBlockHeader{}))
	}
	blockHeader := cmBlockHeader.BlockHeader
	blockHeaderBytes, err := proto.Marshal(blockHeader)
	if err != nil {
		return nil, err
	}
	blockData := storage.NewBlockData(header.GetBlockHash(), blockHeaderBytes)
	return blockData, nil
}

// deserializeBlockHeader deserializes binary bytes to ChainMaker's block header
func (cmc *cmCoder) deserializeBlockHeader(headerBz []byte) (common.Header, error) {
	blockHeader := &cmCommonPb.BlockHeader{}
	err := proto.Unmarshal(headerBz, blockHeader)
	if err != nil {
		return nil, err
	}
	return &chainmaker_common.CMBlockHeader{
		BlockHeader: blockHeader,
	}, nil
}

// generateTransactionData generates transaction data, contains txHashMap, txBytesMap and txRWSetMap
func (cmc *cmCoder) generateTransactionData(blocker common.Blocker,
	genHashMap bool, genTxMap bool, genExtraMap bool) (*storage.TransactionData, error) {
	cmBlock, ok := blocker.(*chainmaker_common.CMBlock)
	if !ok {
		return nil, fmt.Errorf("assert CMBlock failed, get type:{%s}, want type:{%s}",
			reflect.TypeOf(blocker), reflect.TypeOf(chainmaker_common.CMBlock{}))
	}
	td := storage.NewTransactionData()
	txs := cmBlock.Block.Txs
	// generate transaction hash map
	if genHashMap {
		for _, tx := range txs {
			txKey := tx.GetPayload().GetTxId()
			txHash, err := chainmaker_tools.CalcTxHash(tx)
			if err != nil {
				return nil, err
			}
			td.TxHashMap[txKey] = txHash
		}
	}
	// generate transaction map
	if genTxMap {
		for _, tx := range txs {
			txKey := tx.GetPayload().GetTxId()
			txBytes, err := proto.Marshal(tx)
			if err != nil {
				return nil, err
			}
			td.TxBytesMap[txKey] = txBytes
		}
	}
	// generate transaction rw_set map
	rwSet := cmBlock.RWSetList
	if genExtraMap {
		for _, rw := range rwSet {
			txKey := rw.GetTxId()
			rwSetBytes, err := proto.Marshal(rw)
			if err != nil {
				return nil, err
			}
			td.ExtraDataMap[txKey] = rwSetBytes
		}
	}
	return td, nil
}

// deserializeTransaction deserializes the binary bytes of transaction and extra data(rw_set) to transaction
func (cmc *cmCoder) deserializeTransaction(transactionBz []byte, extraDataBz []byte) (
	common.Transactioner, error) {
	tran := &cmCommonPb.Transaction{}
	err := proto.Unmarshal(transactionBz, tran)
	if err != nil {
		return nil, err
	}
	rw := &cmCommonPb.TxRWSet{}
	if extraDataBz != nil {
		err = proto.Unmarshal(extraDataBz, rw)
		if err != nil {
			return nil, err
		}
	}
	return &chainmaker_common.CMTransaction{
		Transaction: tran,
		RWSet:       rw,
	}, nil
}
