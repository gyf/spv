/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

// Package coder defines coding and decoding interface and contains the implementations of each chain
package coder

import (
	"chainmaker.org/chainmaker/spv/v2/common"
	"chainmaker.org/chainmaker/spv/v2/storage"
)

// Coder defines coding and decoding interface
type Coder interface {
	// GenerateBlockData generates transaction data, contains block hash and block header bytes
	GenerateBlockData(header common.Header) (*storage.BlockData, error)

	// DeserializeBlockHeader deserializes binary bytes to block header
	DeserializeBlockHeader(headerBz []byte) (common.Header, error)

	// GenerateTransactionData generates transaction data, contains tx hash map, tx bytes map and
	// extra data(about each tx) bytes map with the txs in block
	GenerateTransactionData(blocker common.Blocker) (*storage.TransactionData, error)

	// DeserializeTransaction deserializes the binary bytes of transaction and extra data to transaction
	DeserializeTransaction(transactionBz []byte, extraDataBz []byte) (common.Transactioner, error)
}
