/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

// Package rpcserver is gRPC server module
package rpcserver

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"net"
	"os"

	"chainmaker.org/chainmaker/spv/v2/conf"
	"chainmaker.org/chainmaker/spv/v2/logger"
	"chainmaker.org/chainmaker/spv/v2/pb/api"
	"chainmaker.org/chainmaker/spv/v2/server"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

// RPCServer is gRPC server module
type RPCServer struct {
	grpcServer *grpc.Server
	spvServer  *server.SPVServer
	ctx        context.Context
	cancel     context.CancelFunc
	log        *zap.SugaredLogger
}

// NewRPCServer creates RPCServer
func NewRPCServer(spvServer *server.SPVServer) (*RPCServer, error) {
	log := logger.GetLogger(logger.ModuleRpc)
	// enable tls
	if conf.SPVConfig.GRPCConfig.EnableTLS {
		var opt credentials.TransportCredentials
		var security = conf.SPVConfig.GRPCConfig.Security
		// enable ca auth
		if security.EnableCertAuth {
			cert, err := tls.LoadX509KeyPair(security.CertFile, security.KeyFile)
			if err != nil {
				log.Errorf("grpc server read cert and key file failed, err:%v", err)
			}
			pool := x509.NewCertPool()
			for _, caFile := range security.CAFile {
				caCertBz, err := os.ReadFile(caFile)
				if err != nil {
					log.Errorf("grpc server read ca file failed,err:%v", err)
				}
				pool.AppendCertsFromPEM(caCertBz)
			}
			cred := credentials.NewTLS(&tls.Config{ // nolint
				Certificates: []tls.Certificate{cert},
				ClientAuth:   tls.RequireAndVerifyClientCert,
				ClientCAs:    pool,
			})
			opt = cred
		} else {
			cred, err := credentials.NewServerTLSFromFile(security.CertFile, security.KeyFile)
			if err != nil {
				log.Errorf("grpc server read cert and key file failed, err:%v", err)
				return nil, err
			}
			opt = cred
		}
		return &RPCServer{
			grpcServer: grpc.NewServer(grpc.Creds(opt)),
			spvServer:  spvServer,
			log:        log,
		}, nil
	}
	// not enable tls
	return &RPCServer{
		grpcServer: grpc.NewServer(),
		spvServer:  spvServer,
		log:        log,
	}, nil
}

// Start startups gRPC Server
func (rs *RPCServer) Start() error {
	var err error
	rs.ctx, rs.cancel = context.WithCancel(context.Background())
	rs.registerHandler()
	conn, err := net.Listen("tcp", conf.SPVConfig.GRPCConfig.ToUrl())
	if err != nil {
		rs.log.Errorf("GRPC Connection Failed! err: %v", err)
		return err
	}
	go func() {
		err = rs.grpcServer.Serve(conn)
		if err != nil {
			rs.log.Errorf("Startup GRPC Server Failed! err: %v", err)
		}
	}()
	rs.log.Infof("GRPC Server Listen on %s", conf.SPVConfig.GRPCConfig.ToUrl())
	return nil
}

// Stop stops gRPC Server
func (rs *RPCServer) Stop() {
	rs.cancel()
	rs.grpcServer.Stop()
	rs.log.Infof("Stop GRPC Server!")
}

// registerHandler registers gRPC Server
func (rs *RPCServer) registerHandler() {
	apiService := NewApiService(rs.ctx, rs.spvServer, rs.log)
	api.RegisterRpcProverServer(rs.grpcServer, apiService)
	api.RegisterRpcForwarderServer(rs.grpcServer, apiService)
}
