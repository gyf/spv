/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package rpcserver

import (
	"context"

	"chainmaker.org/chainmaker/spv/v2/pb/api"
	"chainmaker.org/chainmaker/spv/v2/pb/protogo/cm_pbgo"
	"chainmaker.org/chainmaker/spv/v2/server"
	"go.uber.org/zap"
)

// ApiService contains SPVServer and implements RpcProverServer interface
type ApiService struct {
	spvServer *server.SPVServer
	ctx       context.Context
	log       *zap.SugaredLogger
}

// NewApiService creates ApiService
func NewApiService(ctx context.Context, spvServer *server.SPVServer, log *zap.SugaredLogger) *ApiService {
	apiService := &ApiService{
		spvServer: spvServer,
		ctx:       ctx,
		log:       log,
	}
	return apiService
}

// ValidTransaction is the implementation of RpcProverServer interface
func (s *ApiService) ValidTransaction(ctx context.Context, request *api.TxValidationRequest) (
	*api.TxValidationResponse, error) {
	var (
		response *api.TxValidationResponse
		err      error
	)
	if request == nil {
		response = &api.TxValidationResponse{
			Code:    api.Code_Invalid,
			Message: "TxValidationRequest is nil",
		}
		s.log.Error("RPCServer receives a nil TxValidationRequest")
		return response, nil
	}
	err = s.spvServer.ValidTransaction(request)
	if err != nil {
		response = &api.TxValidationResponse{
			ChainId: request.ChainId,
			TxKey:   request.TxKey,
			Code:    api.Code_Invalid,
			Message: err.Error(),
		}
		s.log.Warnf("RPCServer verifies an invalid transaction, chainId:%s, txKey:%s, err:%v",
			request.ChainId, request.TxKey, err)
		return response, nil
	}
	response = &api.TxValidationResponse{
		ChainId: request.ChainId,
		TxKey:   request.TxKey,
		Code:    api.Code_Valid,
		Message: "valid transaction",
	}
	return response, nil
}

// ForwardRequest is the implementation of ForwardRequest interface
func (s *ApiService) ForwardRequest(ctx context.Context, request *cm_pbgo.TxRequest) (*cm_pbgo.TxResponse, error) {
	// forward request
	response, err := s.spvServer.ForwardRequest(request)
	if err != nil {
		s.log.Errorf("RPCServer forwards request failed, err:%v", err)
		return response, nil
	}
	return response, nil
}
