/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package fabric_verifier

import (
	"testing"

	"chainmaker.org/chainmaker/spv/v2/common/fabric_common"
	"chainmaker.org/chainmaker/spv/v2/utils/fabric_tools"
	"github.com/stretchr/testify/require"
)

func TestFabricVerifier_ValidBlock(t *testing.T) {
	fabVrf := NewFabricVerifier()
	block := fabric_tools.NewBlock()
	fabBlock := &fabric_common.FabBlock{
		ChainId: "mychannel",
		Block:   block,
	}

	err := fabVrf.ValidBlock(fabBlock, []byte("preHash"))
	require.Nil(t, err)
}
