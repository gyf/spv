/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package fabric_verifier

import (
	"bytes"
	"errors"
	"fmt"
	"reflect"

	"chainmaker.org/chainmaker/spv/v2/common"
	"chainmaker.org/chainmaker/spv/v2/common/fabric_common"
	"chainmaker.org/chainmaker/spv/v2/utils/fabric_tools"
	fabCommonPb "github.com/hyperledger/fabric-protos-go/common"
)

// FabricVerifier implements block verifier interface for Fabric
type FabricVerifier struct{}

// NewFabricVerifier creates FabricVerifier
func NewFabricVerifier() *FabricVerifier {
	return &FabricVerifier{}
}

// ValidBlock verifies the validity of the block for Fabric
func (fv *FabricVerifier) ValidBlock(blocker common.Blocker, preBlockHash []byte) error {
	if blocker == nil || preBlockHash == nil {
		return errors.New("blocker or pre block hash is nil")
	}
	fabBlock, ok := blocker.(*fabric_common.FabBlock)
	if !ok {
		return fmt.Errorf("assert FabBlock failed, get type:{%s}, want type:{%s}",
			reflect.TypeOf(blocker), reflect.TypeOf(fabric_common.FabBlock{}))
	}
	return validBlock(fabBlock.Block, preBlockHash)
}

func validBlock(block *fabCommonPb.Block, preBlockHash []byte) error {
	// 1.verify pre hash
	if !bytes.Equal(preBlockHash, block.Header.PreviousHash) {
		return fmt.Errorf("block pre hash is unequal, actual pre hash:%x, pre hash in block:%x",
			preBlockHash, block.Header.PreviousHash)
	}
	// 2. verify data hash
	dataHash := fabric_tools.GetBlockDataHash(block.Data)
	if !bytes.Equal(dataHash, block.Header.DataHash) {
		return fmt.Errorf("block data hash unequal, actual dataHash:%x, dataHash in block:%x",
			dataHash, block.Header.DataHash)
	}
	return nil
}
