/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

// Package verifier defines block verifier interface and contains the implementations of each chain
package verifier

import "chainmaker.org/chainmaker/spv/v2/common"

// Verifier is block verifier interface
type Verifier interface {
	// ValidBlock verifies the validity of the block
	ValidBlock(blocker common.Blocker, preBlockHash []byte) error
}
