/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package chainmaker_verifier

import (
	"testing"

	cmCommonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/spv/v2/common/chainmaker_common"
	"chainmaker.org/chainmaker/spv/v2/conf"
	"chainmaker.org/chainmaker/spv/v2/pb/protogo"
	"github.com/stretchr/testify/require"
)

func TestCMLightVerifier_ValidBlock(t *testing.T) {
	conf.RemoteChainConfigs["chain1"] = &protogo.RemoteConfig{
		HashType: "SHA256",
	}
	tx := &cmCommonPb.Transaction{
		Payload: &cmCommonPb.Payload{
			ChainId: "chain1",
		},
		Result: &cmCommonPb.Result{},
	}
	cml := NewCMLightVerifier()
	err := cml.ValidBlock(&chainmaker_common.CMBlock{
		Block: &cmCommonPb.Block{
			Header: &cmCommonPb.BlockHeader{
				ChainId: "chain1",
				TxCount: 1,
				TxRoot:  []byte{50, 66, 13, 18, 146, 68, 60, 116, 203, 212, 101, 213, 115, 215, 171, 218, 120, 118, 151, 129, 80, 3, 48, 184, 98, 106, 131, 109, 103, 147, 134, 56},
			},
			Txs: []*cmCommonPb.Transaction{
				tx,
			},
		},
	}, []byte{})

	require.Nil(t, err)
}
