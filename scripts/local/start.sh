#!/usr/bin/env bash

#
# Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
#
# SPDX-License-Identifier: Apache-2.0
#

function start_spv() {
  pkill spv
  sleep 1
  nohup ./spv start -c ../config/spv.yml > system.log &
  echo "SPV is Started!"
}

start_spv