#!/usr/bin/env bash

#
# Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
#
# SPDX-License-Identifier: Apache-2.0
#

function stop_spv() {
  pkill spv
  echo "SPV is Stopped!"
}

stop_spv