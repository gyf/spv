#!/usr/bin/env bash

#
# Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
#
# SPDX-License-Identifier: Apache-2.0
#

function start() {
  cd ${DUMP_PATH}/bin && ./start.sh && cd - > /dev/null
}

# output root dir
DUMP_PATH="../build/release"

start