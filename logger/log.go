/*
 Copyright (C) BABEC. All rights reserved.
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package logger

import (
	"sync"

	"go.uber.org/zap"
)

// module log name
const (
	ModuleCli          = "[Cli]"
	ModuleSpv          = "[SPV]"
	ModuleStateManager = "[StateManager]"
	ModuleReqManager   = "[ReqManager]"
	ModuleBlockManager = "[BlockManager]"
	ModuleStateDB      = "[StateDB]"
	ModuleProver       = "[Prover]"
	ModuleRpc          = "[Rpc]"
	ModuleWeb          = "[Web]"
)

var (
	loggers     = make(map[string]*zap.SugaredLogger)
	logConfig   *LogConfig
	loggerMutex sync.Mutex
)

// SetLogConfig sets the config of logger module, called in initialization of config module
func SetLogConfig(config *LogConfig) {
	logConfig = config
}

// GetLogger finds or creates a logger with module name, usually called in initialization of all module.
func GetLogger(name string) *zap.SugaredLogger {
	loggerMutex.Lock()
	defer loggerMutex.Unlock()
	var config *Config
	logHeader := name
	logger, ok := loggers[logHeader]
	if !ok {
		if logConfig == nil {
			logConfig = defaultLogConfig()
		}
		config = &Config{
			Module:       name,
			LogPath:      logConfig.FilePath,
			LogLevel:     GetLogLevel(logConfig.LogLevel),
			MaxAge:       logConfig.MaxAge,
			RotationTime: logConfig.RotationTime,
			JsonFormat:   false,
			ShowLine:     true,
			LogInConsole: logConfig.LogInConsole,
			ShowColor:    logConfig.ShowColor,
		}
		logger = initSugarLogger(config)
		loggers[logHeader] = logger
	}
	return logger
}

// GetDefaultLogger creates default logger
func GetDefaultLogger() *zap.SugaredLogger {
	defaultConfig := defaultLogConfig()
	config := &Config{
		Module:       "[SPV]",
		LogPath:      defaultConfig.FilePath,
		LogLevel:     GetLogLevel(defaultConfig.LogLevel),
		MaxAge:       defaultConfig.MaxAge,
		RotationTime: defaultConfig.RotationTime,
		JsonFormat:   false,
		ShowLine:     true,
		LogInConsole: defaultConfig.LogInConsole,
		ShowColor:    defaultConfig.ShowColor,
	}
	return initSugarLogger(config)
}

// GetCMSDKLogger creates ChainMaker-SDK logger
func GetCMSDKLogger() *zap.SugaredLogger {
	sdkLC := sdkLogConfig()
	config := &Config{
		Module:       "[SDK]",
		LogPath:      sdkLC.FilePath,
		LogLevel:     GetLogLevel(sdkLC.LogLevel),
		MaxAge:       sdkLC.MaxAge,
		RotationTime: sdkLC.RotationTime,
		JsonFormat:   false,
		ShowLine:     true,
		LogInConsole: sdkLC.LogInConsole,
		ShowColor:    sdkLC.ShowColor,
	}
	return initSugarLogger(config)
}

// defaultLogConfig creates default config for logger module
func defaultLogConfig() *LogConfig {
	config := &LogConfig{
		LogLevel:     INFO,
		FilePath:     "../log/default_spv.log",
		MaxAge:       DefaultMaxAge,
		RotationTime: DefaultRotationTime,
		LogInConsole: false,
		ShowColor:    true,
	}
	return config
}

// sdkLogConfig creates ChainMaker-SDK config for logger module
func sdkLogConfig() *LogConfig {
	config := &LogConfig{
		LogLevel:     INFO,
		FilePath:     "../log/chainmaker_sdk.log",
		MaxAge:       DefaultMaxAge,
		RotationTime: DefaultRotationTime,
		LogInConsole: false,
		ShowColor:    true,
	}
	return config
}
